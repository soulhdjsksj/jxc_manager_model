create database JXCSystem
use JXCSystem
create table JXC_Users
(
 UserId int primary key identity (1,1) not null , /*用户id创建，可以有管理员，员工*/
 UserName varchar(5)not null, /*不允许为空*/
 UserNumber varchar(8) not null ,/*进行用户编号*/
 UserPhone varchar(11) not null ,/*不允许电话号码为空*/
 SystemTime smalldatetime not null  ,/*用户登录时间*/
 UserState int  not null ,/*用户状态*/
 Remark varchar(200) not null,/*备注*/
)

create table JXC_Market /*销售表*/
(  Market_Name varchar(5) not null ,/*销售人员名字*/
   UserNumber varchar(8) not null ,/*销售人员既是员工也是后台管理人员*/
   Market_Class nvarchar(10) not null, /*销售类别*/
   Market_record date not null ,/*销售记录*/
   Market_Phone varchar(11) not null ,/*销售手机号*/
   Market_quantity int not null  ,/*销售数量*/
   Remark varchar(200) not null,/*备注*/
)
create table JXC_inventory/*库存表*/
(
GoodsNumber varchar(20) not null ,/*库存数量*/
GoodsClass varchar(20) not null ,/*库存数量*/
GoodsMoney money not null ,/*货物大致价值*/
GoodsCB varchar(20) not null,/*库存品牌*/
)
create table JXC_CompanyPart /*部门表*/
(
  CompanyPartNumber varchar(10) not null primary key  ,/*部门编号*/
  PartNumberPeople varchar(25)not null  ,/*部门人数*/
  PartPrincipal varchar(5) not null , /*部门负责人名字*/
  PartName varchar(8) not null ,/*部门名称*/ 
  Remark varchar(200) not null,/*备注*/

)
create table JXC_CopmanyStaff /*员工表*/
(
  StaffNumber varchar(20) not null  ,/*员工编号*/
  StaffName varchar(20) not null  ,/*员工名字*/
  StaffPhone varchar(11) not null , /*员工电话号码*/
  StaffSex int not null  ,/*员工性别*/
  StaffCode varchar(17) not null  ,/*员工身份证号*/
  StaffDuty varchar(20) not null ,/*员工职务*/
  EntryTime datetime not null ,/*员工入职时间*/
  RegisnTime datetime not null ,/*员工辞职时间*/
  Remark varchar(200) not null,/*备注*/
)
create table JXC_ProductInformation /*商品信息*/
(
 CommodityBrand varchar(20) not null ,/*商品品牌*/
 ProductionID varchar(20) not null ,/*商品Id或者编号*/
 ProductionSource varchar(50) not null ,/*商品原产地进行商品朔源*/
 ProductionClass varchar(20) not null ,/*商品种类*/
 ProductionClassID char(20) not null ,/*商品种类ID*/
 DateofManufacture datetime not null,/*商品生产日期*/ 
 ProductionName varchar(20) not null ,/*商品名称*/
 ProductionQuantity char(20) not null, /*商品数量*/
 ProductionQuality char(10) not null ,/*商品质量*/
 ProductionPurchase datetime  not null ,/*商品引进进货时间*/
 ProductionDeliverTime datetime not null, /*商品出货时间*/
 Remark varchar(200) not null,/*备注*/
 ProductionExpirationdates datetime not null ,/*商品即将过期时间*/
)
create table JXC_WastageGood /*损耗计量表*/
(
ProductionID varchar(20) not null,/*损耗商品的编号*/
WastageGoodClass varchar(20) not null,/*损耗种类*/
WastageGoodQuanlity int not null,/*损耗数量*/
WastageMoney money not null,/*损耗商品合计金额*/
DissPosePrincipal varchar(20) not null ,/*处理负责人*/
)
create table JXC_Stock /*仓库信息*/
(
 Stock_ID int not null primary key , /*仓库id*/
 Stock_Address varchar(50) not null ,/*仓库地址*/
 Stock_Principal varchar(8) not null ,/*仓库负责人*/
 PrincipalPhone varchar(11) not null ,/*仓库负责人电话号码*/
 Stock_Area varchar(20) not null ,/*仓库面积*/
 Storage_Class varchar(20) not null ,/*仓库储物类别*/
 Remark varchar(200) not null,/*备注*/
 
)

create table JXC_Sale /*销售表*/
(
 SaleData datetime not null ,/*销售时间*/
 SaleDataGood varchar(20) not null ,/*销售名称*/
 SaleQuanlity char(20) not null ,/*数量*/
 SaleNumber char(20) not null,/*销售编号*/
 Stock_ID int not null,/*销售仓库ID*/
 Remark varchar(200) not null,/*备注*/
 OrderFormNumber varchar(20) primary key  not null ,/*订单编号主键，且这个是最为重要的.*/
 SaleMoney money not null ,/*销售金额*/
 StaffSale varchar(20) not null ,/*销售员工*/
 SaleSource varchar(20) not null  ,/*销售目的地*/

)
create table JXC_SaleClient /*面向客户出售表*/
(
ClientName varchar(20) not null ,/*客户名字*/
ClientCompany varchar(20) not null ,/*客户所属公司*/
ClientAddress varchar(50) not null ,/*客户所属公司地址*/ 
ClientOrderFormProject varchar(20) not null ,/*订单项目*/
ClientOrderNumber varchar(20) primary key not null ,/*订单编号*/
OrderStartTime datetime not null ,/*订单开始时间*/
ClientExpirationDates datetime not null ,/*订单截止日期*/
ProjectRrincipal varchar(20) not null,/*项目或者订单负责人*/
SaleMoney money not null ,/*销售金额*/
Remark varchar(200) not null ,/*备注*/
)

create table JXC_StockClient /*面向进货商*/
(
StockName varchar(20) not null ,/*进货商名字*/
StockCompany varchar(20) not null ,/*进货商所属公司*/
StockAddress varchar(50) not null, /*进货商所属地*/
StockTime datetime not null ,/*进货时间*/
ProjectPrincipal varchar(20) not null ,/*项目负责人*/
StockNumber nvarchar(20) not null ,/*进货订单编号*/
StockMoney money not null ,/*进货价格*/
Remark varchar(200) not null ,/*备注*/
)
create table JXC_SaleReturn /*退货表*/
(
SaleReturnID varchar(20) not null ,/*退货编号*/
SaleReturnQuanlity varchar(20) not null ,/*退货数量*/
SaleReturnMoney varchar(20) not null,/*退货金额*/
SaleReturnReason nvarchar(300) not null ,/*退货原因*/
ClientName varchar(20) not null ,/*客户退货名*/
ClientCompany varchar(20) not null,/*客户退货所属公司*/
Remark varchar(200) not null ,/*备注*/
)
create table JXC_Contract
(ContractProject nvarchar(20) not null ,/*合同名称*/
ContractID nvarchar(20) primary key  not null,/*合同编号*/
ContractTime nvarchar(20) not null ,/*合同签约时间*/
StockName varchar(20) not null,/*进货商名称*/
CilentName varchar(20) not null,/*客户名字*/
ProjectPrincipal varchar(20) not null,/*项目负责人*/
Remark varchar(200) not null ,/*备注*/
)
