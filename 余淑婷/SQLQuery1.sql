create database users
go
use users
go
create table buyInfo( --进货表 
StockId int primary key identity(1,1),--进货Id
GoodNo varchar(20) not null, --商品编号
supplierNme nvarchar(20) not null, --供应商名称
Price float not null, --进货价格
Number int not null, --进货数量
totalPrice float not null, --商品总价
buyDate datetime not null, --进货时间
) 

create table goodInfo1( --商品信息表
goodNo varchar(20) primary key, --商品编号
goodClassId int not null, --类别Id
goodName nvarchar(30)not null, --商品名称
goodSpecs float not null, --价格
goodPlace nvarchar(50) not null, --产地
)

create table goodStockInfo1( --库存信息表
goodNo varchar(20) primary key, --商品编号
goodCount int not null, --库存数量
)


create table sellInfo1( --商品销售表
sellId int primary key identity(1,1), --销售id
goodNo varchar(20) not null, --商品编号
Price float not null, --单价
Number int not null, --数量
sellTime datetime not null, --售出时间
)


create table supplierInfo(--供应商信息表
supplierName nvarchar(50) not null, --供应商名称
supplierLawyer nvarchar(4) not null, --法人代表
supplierTelephone varchar(11) not null, --电话
supplierAddress nvarchar(50) not null, --地址
)

--进货表插入数据
select * from buyInfo
insert into buyInfo ( GoodNo, supplierNme, Price,Number,totalPrice,buyDate) 
	values ('01', '**供应商', '10', '10','100',GETDATE());
insert into buyInfo ( GoodNo, supplierNme, Price,Number,totalPrice,buyDate) 
	values ('02', '&&供应商', '9', '9','81',GETDATE());
insert into buyInfo ( GoodNo, supplierNme, Price,Number,totalPrice,buyDate) 
	values ('03', '^^供应商', '5', '10','50',GETDATE());
insert into buyInfo ( GoodNo, supplierNme, Price,Number,totalPrice,buyDate) 
	values ('04', '$$供应商', '10', '15','150',GETDATE());

--商品信息表插入数据
select * from goodInfo1
insert into goodInfo1 ( goodNo, goodClassId, goodName,goodSpecs,goodPlace) 
	values ('011', '1', '辣条', '10','青青草原');
insert into goodInfo1 ( goodNo, goodClassId, goodName,goodSpecs,goodPlace) 
	values ('022', '2', '圆规', '9','老八草原');
insert into goodInfo1 ( goodNo, goodClassId, goodName,goodSpecs,goodPlace) 
	values ('033', '3', '羽毛球', '5','灰太狼城堡');
insert into goodInfo1 ( goodNo, goodClassId, goodName,goodSpecs,goodPlace) 
	values ('044', '4', '洗衣液', '10','团结里');

--库存表插入数据
select * from goodStockInfo1
insert into goodStockInfo1(goodNo,goodCount ) 
	values ('01','10');
insert into goodStockInfo1(goodNo,goodCount ) 
	values ('02','9');
insert into goodStockInfo1(goodNo,goodCount ) 
	values ('03','10');
insert into goodStockInfo1(goodNo,goodCount ) 
	values ('04','5');

--销售表插入数据
select * from sellInfo1
insert into sellInfo1(goodNo,Price,Number,sellTime) 
	values ('01','10','10',GETDATE());
insert into sellInfo1(goodNo,Price,Number,sellTime) 
	values ('02','9','9',GETDATE());
insert into sellInfo1(goodNo,Price,Number,sellTime) 
	values ('03','5','10',GETDATE());
insert into sellInfo1(goodNo,Price,Number,sellTime) 
	values ('04','10','15',GETDATE());
 
--法人表插入数据
select * from supplierInfo
insert into supplierInfo(supplierName,supplierLawyer,supplierTelephone,supplierAddress)
	values('**供应商','红太狼','4008238823','城堡')
insert into supplierInfo(supplierName,supplierLawyer,supplierTelephone,supplierAddress)
	values('&&供应商','红太狼','4008238823','城堡02')