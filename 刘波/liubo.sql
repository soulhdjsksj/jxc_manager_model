create database  GoodsInAndOut

go


--用户权限
create table UserRight 
(
	UserRightlevel int,--权限等级
	UserRightFunction varchar(100) not null,--权限功能
)

--用户表
create table Users
(
	UsersId int primary key identity,
	UsersName varchar(20) not null,
	UsersPassWord varchar(20) not null,
	UserRightlevel int,
)

--商品类型
create table ProductType
(
	ProductTypeId int primary key identity,
	TypeName varchar(20) not null,
)

--商品状态
create table ProductState
(
	ProductStateID int not null,
	ProductStates varchar(20) not null,
)

--所有商品
create table AllProduct
(
	AllProductId int primary key identity,
	ProductTypeId int not null,
	ProductName varchar(20) not null,
	ProductStateID int not null, --商品状态
	StockId int not null,
	ShipmentId int,
	ProductCode varchar(30) not null
)

--供货商
create table Supplier
(
	SupplierId int,
	SupplierName varchar(20), 
)


--进货表
create table Stock
(
	StockId int primary key identity,
	SupplierId int,
	ProductTypeId int not null,
	ProductName varchar(20) not null, --商品名
	StockPrice money not null,
	PurchaseTime smalldatetime not null,
)

--进货退货表
create table StockOut
(
	StockOutId int primary key identity,
	ProductTypeId int not null,
	ProductName varchar(20) not null, --商品名
	AllProductId int not null,
	OutTime smalldatetime not null,
)

--货架表
create table GoodsShelf
(
	GoodsShelfId int not null,
	SubdomainId int not null,
	ProductTypeId int not null,
)


--现货表
create table CashCommodity
(
	CashCommodityId int primary key identity,
	StockId int not null, --进货
	GoodsShelfId int not null,--大货架
	SubdomainId int not null,--小货架
	ProductTypeId int not null,
	AllProductId int not null,
	InventoryTime smalldatetime not null,--存货时间
) 

--订单表
create table Indent
(
	IndentId int primary key identity,
	CustomerName nvarchar(20) not null,
	CashCommodityId int not null, --现货Id
	PricePaid money not null,
	IndentAddress nvarchar(100) not null,
)

--取消订单
create table EscIndent
(
	EscIndentId int primary key identity,
	CustomerName nvarchar(20) not null,
	CashCommodityId int not null, --现货Id
	Cause varchar(100) not null --原因
)

--出货表

create table Shipment
(
	ShipmentId int primary key identity,
	IndentId int not null,
	ProductTypeId int not null,
	AllProductId int not null,
	ShipmentTime smalldatetime not null,
)


--盈利表
create table Payoff
(
	AllProductId int not null,
	StockPrice money not null,--进货价
	PricePaid money not null,--出货价
)

--出货退货表
create table ShipmentOut
(
	ShipmentOutId int primary key identity,
	ProductName varchar(20) not null, --商品名
	ProductTypeId int not null,
	AllProductId int not null,
	IndentId int not null, --订单Id
	OutTime smalldatetime not null
)

--部门表
create table Department
(
	DepartmentId int not null,
	DepartmentName varchar(20) not null,
)

--员工表

create table Staff
(
	StaffId  int primary key identity,
	StaffName varchar(20) not null,
	Sex varchar(20) not null,
	Age int not null,
	DepartmentId int not null,
	PositionId int,
)

--职位表
create table Position
(
	PositionId int,
	PositionName varchar(20) not null,
)