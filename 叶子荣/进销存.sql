create database JXC

use JXC

create table Users		--用户表
(
	Users_Id int identity(1,1) primary key not null,
	Users_Code varchar(30) not null,
	Users_Sex varchar(1) not null,
	Users_Brith smalldatetime not null,
	Users_Phone varchar(20) not null,
	Users_name varchar(20) not null,
	Users_Pwd varchar(10) not null ,
	Users_State int check(Users_State = 1 or Users_State = 0) default(1) --逻辑删除 1表示该用户正常，0表示该用户已被删除
)
create table Supplier	--供应商表
(
	Supplier_Id int identity(1,1) primary key not null,
	Supplier_Name nvarchar(20) not null,
	Supplier_Address nvarchar(50) not null,
	Contacts nvarchar(20) not null,
	Contacts_Phone varchar(20),
	Postcode varchar(20)
)

create table  Client		--客户表
(
	Client_Id int primary key identity(1,1) not null,
	Client_Name nvarchar(20) not null,
	Client_Address nvarchar(50) not null,
	Contacts nvarchar(20) not null,
	Contacts_Phone nvarchar(20) not null,
	Postcode varchar(20) not null,
	Client_State int check(Client_State = 1 or Client_State = 0) default(1) --逻辑删除 1表示该客户正常，0表示该客户已被删除
)

create table Dept			--部门表
(	
	Dept_Id int primary key identity(1,1) not null,
	Dept_Name nvarchar(20) not null,
	Dept_Address nvarchar(20) not null,
)

create table Dept_Supplier --部门—供应商表
(
	Dept_Supplier_Id int identity(1,1) primary key not null,
	Dept_Id int,  --部门编号  外键 来自部门表部门编号字段
	Supplier_Id int --供应商编号  外键 来自供应商表ID字段
)
alter table Dept_Supplier add constraint  Fk_Dept_Id foreign key (Dept_Id) references Dept(Dept_Id)
alter table Dept_Supplier add constraint  Fk_Supplier_Id foreign key (Supplier_Id) references Supplier(Supplier_Id)

create table Dept_Client	--部门——客户表
(
	Dept_Client_Id int identity(1,1) primary key not null,
	Dept_Id int, --部门编号  外键 来自部门表部门编号字段
	Client_Id int --客户编号 外键 来自客户表客户ID
)
alter table Dept_Client add constraint Fk_Dept_Client_Dept_Id foreign key (Dept_Id) references Dept(Dept_Id)
alter table Dept_client add constraint Fk_Dept_Client_Client_Id foreign key (Client_Id) references Client(Client_Id)

create table Warehouse
(
	Warehouse_Id int primary key identity(1,1) not null, 
	Warehouse_Address nvarchar(30) not null,
	Storekeeper int, --外键  来自员工表 员工编号
	Warehouse_Phone varchar(30)
)
alter table Warehouse add constraint Fk_Warehouse_Storekeeper foreign key(Storekeeper) references Employee(Employee_Id)


create table GeneralClassificationOfGoods	--商品总分类表
(
	CommodityClassification_Id int primary key identity(1,1) not null,	--商品分类编号
	SystematicName nvarchar(30) not null,								--分类名称
	StartedTheirClassification int not null,					--建分类人 外键 来自员工表员工编号
)
alter table GeneralClassificationOfGoods add constraint Fk_GeneralClassificationOfGoods_StartedTheirClassification 
					foreign key (StartedTheirClassification) references Employee(Employee_Id)



create table BreakdownOfGoods    --商品细分类表
(
	CommodityClassification_Id int not null,  --商品总分类编号 外键 来自商品总分类表 商品分类编号
	BreakdownOfGoods int primary key identity(1,1) not null, --商品细分类编号 主键
	Commodity_Name nvarchar(30) not null,
	StartedTheirClassification int not null,				--建分类人 外键 来自员工表员工编号
)
alter table BreakdownOfGoods add constraint Fk_BreakdownOfGoods_CommodityClassification_Id 
			foreign key (CommodityClassification_Id) references GeneralClassificationOfGoods (CommodityClassification_Id)

alter table BreakdownOfGoods add constraint Fk_BreakdownOfGoods_StartedTheirClassification
			foreign key (StartedTheirClassification) references Employee(Employee_Id)



create table CommoditySpecificationSheet --商品规格表
(
	CommoditySpecificationSheet_Id int primary key identity(1,1) not null,
	CommoditySpecificationSheet_Name varchar(20) not null, --商品规格
	Operator int not null, --操作员 来自员工表员工编号
	OperatorTime smalldatetime not null -- 操作时间
)
alter table CommoditySpecificationSheet add constraint Fk_CommoditySpecificationSheet_Operator 
			foreign key (Operator) references Employee(Employee_Id)



create table CommodityUnitOfMeasurement --商品计量单位表
(
	CommodityUnitOfMeasurement_Id int primary key identity(1,1) not null,
	CommodityUnitOfMeasurement_Name varchar(20) not null,	--计量单位名称
	Operator int not null , --操作员 来自员工表员工编号
	Create_Time smalldatetime--创建时间
)
alter table CommodityUnitOfMeasurement add constraint Fk_CommodityUnitOfMeasurement_Operator 
			foreign key (Operator) references Employee(Employee_Id)


create table Catalogue--商品目录表
(
	BreakdownOfGoods_Id int not null, --外键来自商品细分类表商品细分类编号
	Product_Id int primary key identity(1,1) not null, --商品编号
	Product_Name nvarchar(30) not null,
	CommoditySpecificationSheet_Id int not null, --商品规格 外键 来自 来自商品规格表 规格编号
	CommodityUnitOfMeasurement_Id int not null, --商品计量单位 外键 来自商品计量单位表 编号
	Price money default(0),
	Product_State int check(Product_State = 1 or Product_State = 0) default(1) ----逻辑删除 1表示该商品正常，0表示该商品已被下架
)
alter table Catalogue add constraint Fk_Catalogue_BreakdownOfGoods 
		foreign key(BreakdownOfGoods_Id) references BreakdownOfGoods(BreakdownOfGoods)
alter table Catalogue add constraint Fk_Catalogue_CommoditySpecificationSheet_Id 
		foreign key(CommoditySpecificationSheet_Id) references CommoditySpecificationSheet(CommoditySpecificationSheet_Id)



create table Product_Supplier  --商品—供应商表
(
	Product_Id int primary key not null, --商品编号 外键来自商品目录表 商品编号
	Supplier_Id int not null, --供应商 外键 来自供应商表 供应商编号
)
alter table Product_Supplier add constraint Fk_Product_Supplier_Product_Id foreign key(Product_Id) references Catalogue(Product_Id)
alter table Product_Supplier add constraint Fk_Product_Supplier_Supplier_Id foreign key (Supplier_Id) references Supplier(Supplier_Id)

create table Employee  --员工表
(
	Employee_Id int primary key identity(1,1) not null,
	Employee_Name nvarchar(30) not null,
	Employee_Sex varchar(1) check(Employee_Sex = '男' or Employee_Sex = '女') default('男'),
	Department_Id int not null, --外键 来自部门表部门编号
	Job varchar(20) not null, 
	Brith datetime not null,
	Code varchar(30) not null,
	Email nvarchar(30) not null,
	Employee_State int check(Employee_State = 1 or Employee_State = 0) --逻辑删除 1表示该员工在职，0表示该员工已离职
)
alter table Employee add constraint Fk_Employee_Department_Id foreign key(Department_Id) references Dept(Dept_Id)

create table ThePurchaseContract  --进货合同表
(
	ThePurchaseContract_Id int primary key identity(1,1) not null,--进货合同编号
	ThePurchaseContract_SigningTime smalldatetime not null, --合同签订日期
	ThePurchaseContract_Effective smalldatetime  not null, --合同生效日期
	ThePurchaseContract_EndTime smalldatetime not null, --合同到期日期
	Signing_Dept int not null,--签订部门 外键 来自部门表部门编号
	Supplier_Id int not null, --供应商编号 外键 来自供应商表供应商编号
	ContractManager int not null, --合同负责人 外键 来自员工表员工编号 
)
alter table ThePurchaseContract add constraint Fk_ThePurchaseContract_Signing_Dept foreign key(Signing_Dept) references Dept(Dept_Id)
alter table ThePurchaseContract add constraint Fk_ThePurchaseContract_Supplier_Id foreign key (Supplier_Id) references Supplier(Supplier_Id)
alter table ThePurchaseContract add constraint Fk_ThePurchaseContract_ContractManager foreign key (ContractManager) references Employee(Employee_Id)

create table DetailsOfPurchaseContract --进货合同明细表
(
	DetailsOfPurchaseContract_Id int primary key not null, --进货合同明细编号 外键 来自进货合同表进货合同编号
	Product_Id int not null, --商品编号 外键 来自商品目录表商品编号
	Product_Amount int not null, --商品数量
	Product_PurchasingPrice money, --商品进价
)
alter table DetailsOfPurchaseContract add constraint Fk_DetailsOfPurchaseContract_DetailsOfPurchaseContract_Id
			foreign key (DetailsOfPurchaseContract_Id) references ThePurchaseContract(ThePurchaseContract_Id)

alter table DetailsOfPurchaseContract add constraint Fk_DetailsOfPurchaseContract_Product_Id
			foreign key (Product_Id) references Catalogue(Product_Id)

create table GodownEntry --入库单
(
	GodownEntry_Id int primary key identity(1,1) not null, --入库单号
	GodownEntryTime smalldatetime  not null, --入库时间
	GodownEntry_Dept int not null, --入库部门 外键来自部门表部门编号
	Warehouse_Id int not null,--外键 来自仓库表仓库编号
	WarehouseEmployee_id int not null --外键  来自员工表员工编号 
)
alter table GodownEntry add constraint Fk_GodownEntry_GodownEntry_Dept foreign key(GodownEntry_Dept) references Dept(Dept_Id)
alter table GodownEntry add constraint Fk_GodownEntry_Warehouse_Id	foreign key(Warehouse_Id) references Warehouse(Warehouse_Id)


create table DetailsOfWarehousingEntry --入库单明细
(
	GodownEntry_Id int primary key not null, --入库单号 外键 来自入库单表入库单号
	Product_Id int not null,--商品编号 外键 来自商品目录表商品编号
	Product_Amount int not null, --商品数量
	Product_Price money, --商品价格
	Invoice int check(Invoice = 1 or Invoice = 0 )default(1) not null,   --有无发票 0表示没有发票1表示有发票
	InvoiceNumber varchar(30) default('********************') --发票号
)
alter table DetailsOfWarehousingEntry add constraint Fk_DetailsOfWarehousingEntry_GodownEntry_Id foreign key(GodownEntry_Id) 
				references GodownEntry(GodownEntry_Id)

alter table DetailsOfWarehousingEntry add constraint Fk_DetailsOfWarehousingEntry_Product_Id foreign key(Product_Id)
				references Catalogue(Product_Id)

create table OutWarehouse --出库单表
(
	OutWarehouse_Id int primary key identity(1,1) not null, --出库单表编号
	OutWarehouse_Time smalldatetime not null, --出库时间
	OutWarehouseDept int not null, --出库部门 外键 来自部门表部门编号
	OutWarehouse int not null, --所出仓库 外键 来自仓库表仓库编号
	OutWarehouse_Emplooye int not null,--出库人 外键 来自员工表员工编号
)
alter table OutWarehouse add constraint Fk_OutWarehouse_OutWarehouseDept foreign key(OutWarehouseDept)
			references Dept(Dept_Id)

alter table OutWarehouse add constraint Fk_OutWarehouse_OutWarehouse foreign key(OutWarehouse)
			references Warehouse(Warehouse_Id)

alter table  OutWarehouse add constraint Fk_OutWarehouse_OutWarehouse_Emplooye foreign key(OutWarehouse_Emplooye)
			references Employee(Employee_Id)



create table OutWarehouseDetail --出库单明细
(
	OutWarehouse_Id int primary key not null,--出库单编号 主键 外键 来自出库单表出库单编号
	Product_Id int not null,--商品编号 外键 来自商品目录表商品编号
	OutWarehouseNumber int not null, --出库数量
	OutWarehousePrice money not null --出库价格
)
alter table OutWarehouseDetail add constraint Fk_OutWarehouseDetail_OutWarehouse_Id foreign key(OutWarehouse_Id)
		references OutWarehouse(OutWarehouse_Id)

alter table OutWarehouseDetail add constraint Fk_OutWarehouseDetail_Product_Id foreign key(Product_Id)
		references Catalogue(Product_Id)



create table ReturnsNote	--退货单表
(
	ReturnsNote_Id int primary key identity(1,1) not null,--退货单表编号
	ReturnsNote_Time int not null, --退货日期
	ReturnsNote_Dept int, --退货部门 外键 来自部门表部门编号
	ReturnsWarehouse int, -- 退入仓库 外键 来自仓库表仓库编号
	ReturnsEmplooye int, --退货人 外键 来自员工表员工编号
	ReturnsWhy nvarchar(200) --退货原因
)
alter table ReturnsNote add constraint Fk_ReturnsNote_ReturnsNote_Dept foreign key(ReturnsNote_Dept)
		references Dept(Dept_Id)

alter table ReturnsNote add constraint Fk_ReturnsNote_ReturnsWarehouse foreign key(ReturnsWarehouse)
		references Warehouse(Warehouse_Id)

alter table ReturnsNote add constraint Fk_ReturnsNote_ReturnsEmplooye foreign key(ReturnsEmplooye)
		references Employee(Employee_Id)


create table SalesContract  --销售合同表
(
	Contract_Id int primary key identity(1,1) not null, --合同编号
	Contract_SigningTime smalldatetime not null, --合同签订日期
	Contract_Effective smalldatetime  not null, --合同生效日期
	Contract_EndTime smalldatetime not null, --合同到期日期
	Department int  not null, --签订部门 外键 来自部门表部门编号
	Client_Id int not null,--客户编号 外键 来自客户表客户编号
	ContractManager int not null,--合同负责人 外键 来自员工表员工编号
)
alter table SalesContract add constraint Fk_SalesContract_Department foreign key(Department)
		references Dept(Dept_Id)

alter table SalesContract add constraint  Fk_SalesContract_Client_Id foreign key (Client_Id)
		references Client(Client_Id)

alter table SalesContract add constraint   Fk_SalesContract_ContractManager foreign key(ContractManager)
		references Employee(Employee_Id)

create table SalesContractDetail --销售合同明细表
(
	Contract_Id int primary key not null,--销售合同编号 主键 外键 来自销售合同表合同编号
	Product_Id int not null,--商品编号 外键 来自商品目录表商品编号
	Product_Amount int not null, --商品数量
	Product_PurchasingPrice money, --商品进价
)

alter table SalesContractDetail add constraint Fk_SalesContractDetail_Contract_Id
			foreign key(Contract_Id) references SalesContract(Contract_Id)

alter table SalesContractDetail add constraint Fk_SalesContractDetail_Product_Id
			foreign key(Product_Id) references Catalogue(Product_Id)


create table StockIn		--进货表
(
	StockIn int primary key identity(1,1) not null,--进货编号 主键
	StockInTime smalldatetime not null,--进货日期
	StockInDept int not null,--进货部门 外键  来自部门表部门编号
	Examiner int not null,--验货人 外键 来自员工表员工编号
)
alter table StockIn add constraint Fk_StockIn_StockInDept foreign key(StockInDept)
		references Dept(Dept_Id)

alter table StockIn add constraint Fk_StockIn_Examiner foreign key(Examiner)
		references Employee(Employee_Id)
	
create table StockInDetail	--进货明细表
(
	StockIn int primary key not null,--进货编号 主键 外键 来自进货表进货编号
	Product_Id int not null,--商品编号 外键 来自商品目录表商品编号
	ThePurchaseContract_Id int not null,--采购合同 外键 来自合同表合同编号
	Amount int not null, --数量
	Price money not null,--价格
)
alter table StockInDetail add constraint Fk_StockInDetail_StockIn foreign key(StockIn)
		references StockIn(StockIn)

alter table StockInDetail add constraint Fk_StockInDetail_Product_Id  foreign key(Product_Id)
		references  Catalogue(Product_Id)

alter table StockInDetail add constraint Fk_StockInDetail_ThePurchaseContract_Id foreign key(ThePurchaseContract_Id)
		references DetailsOfPurchaseContract(DetailsOfPurchaseContract_Id)

create table Market  --销售表
(
	Market_Id int primary key identity(1,1) not null,--销售编号 主键
	MarketTime smalldatetime not null,--销售日期
	MarketDept int not null,--销售部门 外键 来自部门表部门编号
	Seller int not null,--售货人 外键 来自员工表员工编号
)
alter table Market add constraint Fk_Market_MarketDept foreign key(MarketDept)
		references Dept(Dept_Id)

alter table Market add constraint Fk_Market_Market_Seller foreign key (Seller)
		references Employee(Employee_Id)
create table MarketDetail  --销售明细表
(
	Market_Id int primary key not null,--销售编号 主键 外键 来自销售表销售编号
	Product_Id int not null,--商品编号 外键 来自商品目录表商品编号
	Contract_Id int  not null,--销售合同 外键 来自合同表合同编号
	Amount int not null,--数量
	Price money not null,--价格
	Discount int not null --折扣
)
alter table MarketDetail add constraint Fk_MarketDetail_Market_Id foreign key(Market_Id)
		references Market(Market_Id)

alter table MarketDetail add constraint Fk_MarketDetail_Product_Id foreign key(Product_Id)
		references Catalogue(Product_Id)

alter table MarketDetail add constraint Fk_MarketDetail_Contract_Id foreign key(Contract_Id)
		references SalesContract(Contract_Id)
		

create table Inventory  --库存表
(
	Inventory_Id int primary key identity(1,1) not null,
	Inventory_Dept int not null,--商品所属部门 外键 来自部门表部门编号
	Inventory_Warehouse int not null,--所在仓库 外键 来自仓库表仓库编号
	Product_Id int not null,--商品编号 外键 来自商品目录表商品编号
	TheProductFirstTimeGodownEntry smalldatetime not null,--此种商品第一次入库时间 
	TheProductLastTimeOutWarehouse smalldatetime not null,--此种商品最后一次出库时间
	Amount int not null, --数量
	WeightedPrice money default(0) --加权价
)
alter table Inventory add constraint Fk_Inventory_Inventory_Dept foreign key(Inventory_Dept)
		references Dept(Dept_Id)

alter table Inventory add constraint Fk_Inventory_Inventory_Warehouse foreign key(Inventory_Dept)
		references Warehouse(Warehouse_Id)

alter table Inventory add constraint Fk_Inventory_Inventory_Product_Id foreign key(Product_Id)
		references Catalogue(Product_Id)