create database Market
go
use	Market
go
--员工表
create table Staff (
	StaffId			int  primary key identity(1,1) ,	--员工id
	StaffNum		int			not null,				--员工编号
	StaffName		varchar(20) not null,				--员工姓名
	StaffPhone      varchar(20) not null,				--员工电话
)

--用户表
create table UserInfo(
	UserId			int primary key identity(1,1),		--用户id
	UserName		varchar(20) not null ,				--用户名
	UserPwd			varchar(10)	not null ,				--密码
	UserPhone		varchar(20) not null ,				--电话号码	 
)

--供货商表
create table Supplier(
	SupplierId			int primary key identity(1,1),  --供货商id
	SupplierName		varchar(20)			not null,	--供货商名称
	SupplierAddress		varchar(50)			not null,	--地址
	SupplierPhone		varchar(20)			not null,	--电话号码
)

--客户表
create table Customer(
	CustomerId			int primary key identity(1,1),	--客户id
	CustomerName		varchar(10)		not null,		--客户姓名
	CustomerPhone		varchar(20)		not null,	    --联系电话
	CustomerAddress		varchar(50)		not null,		--地址
)

--商品表
create table Commodity (
	CommodityId			int primary key identity(1,1),	--商品id
	CommodityNum		int				  not null,     --商品编号
	CommodityName		varchar(30)	      not null,	  	--商品名称
	CommodityPrice		money			  not null,		--商品价格
	CommodityType		varchar(20)       not null,		--商品类型	
)

--库存表
create table Repertoy(
	RepertoyId			int primary key identity(1,1),	
	StoeCommodityId		int			   	  not null,	 	--商品编号
	CommodityType		varchar(20)       not null,		--商品类型
	CommodityNum		int				  not null,		--商品数量
	StoreTime			smalldatetime	  not null,		--入库时间
)



--进货表（入库，付款）
create table Purchase(
	CommodityNum		int	primary key	  not null,     --商品编号
	CommodityName		varchar(30)	      not null,	    --商品名称
	SupplierId			int				  not null,     --供货商编号
	SupplierName		varchar(20)		  not null,  	--供货商名称
	TransactionTye      varchar(30)		  not null,		--交易类型	
	TransTime			smalldatetime     not nulL,		--交易时间
	TradingVolume		money			  not null,	    --成交金额
	ShippingAddress		varchar(50)		  not null,     --发货地址
	RecevivingAdderss   varchar(50)		  not null,		--收货地址	
)

--退货表
create table StorageInReturn(
	CommodityNum		int	primary key	  not null,     --商品编号
	CommodityName		varchar(30)	      not null,	    --商品名称
	SupplierId			int				  not null,     --供货商编号
	SupplierName		varchar(20)		  not null,  	--供货商名称
	StorReturnWhy		varchar(40)       not null,		--退款原因
	StorReturnMoney		money			  not null,		--退款金额	
	ShippingAddress		varchar(50)		  not null,     --发货地址
	RecevivingAdderss   varchar(50)		  not null,		--收货地址	
	StorReturnTime		smalldatetime     not null,     --退款时间
)

--出货表(出库，收款)
create table Trans(
	CommodityNum		int	primary key	  not null,     --商品编号
	CommodityName		varchar(30)	      not null,	    --商品名称
	TransactionTye      varchar(30)		  not null,		--交易类型	
	TransTime			smalldatetime     not nulL,		--交易时间
	TradingVolume		money			  not null,	    --成交金额
	ShippingAddress		varchar(50)		  not null,     --发货地址
	RecevivingAdderss   varchar(50)		  not null,		--收货地址	
)
