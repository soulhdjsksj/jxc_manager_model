create database Store
go
use Store
go

create table Users	--用户表
(
	UserId int primary key identity(1,1),
	UserPwd varchar(8),
	UserName varchar(10),
	UserPhone varchar(20),
	UserEmail varchar(20)
)


create table Employee	--员工表   
(   
  EmployeeId int primary key identity(1,1) NOT NULL, --员工编号   
  DeptId int NOT NULL,	--所属部门编号   
  Name varchar(30) NOT NULL,	--姓名  
  Duty varchar(20) NOT NULL,	--职务 
  Gender varchar(6) NOT NULL,	--性别   
  BirthDate datetime NOT NULL,	--出生日期    
  IdentityCard varchar(20) NULL,--身份证号  
  Address varchar(250) NULL,    --住址  
  Phone varchar(25) NULL,       --电话  
  Email varchar(30) NULL        --E_MAIL  
)  

create table Supplier
(
	SupplierId int primary key identity(1,1) not null,	--供应商表
	Name varchar(250),
	Address varchar(250),
	Phone varchar(20),
	PostalCode varchar(10),	--邮编
	ConstactPerson varchar(10)	--联系人
)

create table Customer	--客户表
(
	CustomerId int primary key identity(1,1) not null,
	Name varchar(250),
	Address varchar(250),
	Phone varchar(20),
	PostalCode varchar(10),	--邮编
	ConstactPerson varchar(10)	--联系人
)

create table Dept	--部门表
(
	DeptId int primary key identity(1,1) not null,
	Name varchar(30)
)

create table StoreHouse	--仓库表
(
	StoreHouseId int primary key identity(1,1),	--仓库编号
	Address varchar(250),
	Phone varchar(20),
	EmployeeId int constraint FK_StoreHouse_Employee_EmployeeId foreign key references Employee(EmployeeId),	--仓库管理人员（外键Employee员工表EmployeeId）
	CreateDate datetime	--建库时间
)

create table ProductClass	--商品总分类表
(
	ProductClassId int primary key identity(1,1) not null,	--商品总分类编号
	Name varchar(30),	--商品分类名称
	EmployeeId int,	--建分类人
	createdate datetime	--建分类时间
)

create table ProductList	--商品细分类表
(
	ProductClassId int foreign key references ProductClass(ProductClassId),	--外键商品总分类表ProductClass
	ProductListId int primary key identity(1,1) not null,	--主键,商品细分类编号
	Name varchar(30),
	EmployeeId int constraint FK_ProductList_Employee_EmployeeId foreign key references Employee(EmployeeId),	--建分类人 外键Employee员工表EmployeeId员工id
	createdate datetime,	--建分类时间
)

create table Product	--商品目录表
(
	ProductListId int constraint FK_Product_ProductList_ProductListId foreign key references ProductList(ProductListId),	--商品细分类编号 外键ProductList商品细分类表ProductListId
	ProductId int primary key identity(1,1),	--商品编号，主键
	Name varchar(30),	--商品名称
	Price money,	--商品价格
	EmployeeId int constraint FK_Product_Employee_EmployeeId foreign key references Employee(EmployeeId),	--目录操作员 外键Employee员工表EmployeeId员工id
	createdate datetime	--创建时间
)

create table BuyTable	--进货单表
(
	BuyTableId int primary key identity(1,1),	--进货单编号
	SupplierId int constraint FK_BuyTable_Supplier_SupplierId foreign key references Supplier(SupplierId),	--供应商 外键Supplier供应商表SupplierId供应商id
	EmployeeId int constraint FK_BuyTable_Employee_EmployeeId foreign key references Employee(EmployeeId),	--负责人 外键Employee员工表EmployeeId员工id
)

create table BuyTableMingxi	--进货单明细表
(
	BuyTable int constraint FK_BuyTableMingxi_BuyTable_BuyTableId foreign key references BuyTable(BuyTableId),	--进货单号 外键BuyTable进货单表BuyTableId进货单编号
	ProductId int constraint FK_BuyTableMingxi_Product_ProductId foreign key references Product(ProductId),	--进货商品编号 外键Product商品目录表ProductId商品编号
	Number int,	--商品数量
	Price money	--商品价格
)

create table InStock	--入库单表
(
	InStockId int primary key identity(1,1) not null,	--入库单编号
	InStockDate datetime,	--入库时间
	DeptId int constraint FK_InStock_Dept_DeptId foreign key references Dept(DeptId),	--入库部门 外键Dept部门表DeptId部门id
	StoreHouseId int constraint FK_InStock_StoreHouse_StoreHouseId foreign key references StoreHouse(StoreHouseId),	--所入仓库编号
	EmployeeId int constraint FK_InStock_Employee_EmployeeId foreign key references Employee(EmployeeId)	--入库操作人员员工编号 外键Employee员工表EmployeeId员工id
)

create table InStockMingxi	--入库单明细   
(   
  InStockId int constraint FK_InStockMingxi_InStock_InStockId foreign key references InStock(InStockId) NOT NULL,	--入库单编号 , 主键, 外键 (InStock入库单表InStockId入库单编号) 
  ProductId int constraint FK_InStockMingxi_Product_ProductId foreign key references Product(ProductId) NOT NULL,  --此种商品编号,主键, 外键 (Product商品目录表ProductId商品编号)    
  Number int NOT NULL,	--此种商品数量   
  Price money ,				--此种商品参考价格    
)  

create table OutStock	--出库单表
(
	OutstockId int primary key identity(1,1) not null,	--出库单编号
	OutTime datetime,	--出库时间
	DeptId int constraint FK_OutStock_Dept_DepId foreign key references Dept(DeptId) not null,	--出库部门 
	StoreHouseId int constraint FK_OutStock_StoreHouse_StoreHouseId foreign key references StoreHouse(StoreHouseId),	--所出仓库 外键StoreHouse仓库表StoreHouseId仓库编号
	ToStoreHouseId int constraint FK_OutStock_ToStoreHouse_StoreHouseId foreign key references StoreHouse(StoreHouseId),	--所入仓库 外键StoreHouse仓库表StoreHouseId仓库编号
	EmployeeId int constraint FK_OutStock_Employee_EmployeeId foreign key references Employee(EmployeeId)	--出库人 外键Employee员工表EmployeeId员工id
)

create table OutStockMingxi	--出库单明细表
(
	OutstockId int constraint FK_OutStockMingxi_OutStock_OutstockId foreign key references Outstock(OutstockId),	--出库单编号 外键Outstock出库单表OutstockId出库编号
	ProductId int constraint FK_OutStockMingxi_Product_ProductId foreign key references Product(ProductId),			--出库商品编号 外键Product商品目录表ProductId商品编号
	Number int,	--出库此商品的数量
	Price money	--出库价钱
)

create table Sale	--销售单表
(
	SaleId int primary key identity not null,	--销售单编号
	SaleDate datetime,	--销售日期
	DeptId int constraint FK_Sale_Dept_DeptId foreign key references Dept(DeptId),	--销售部门 外键Dept部门表DeptId部门id
	EmployeeId int constraint FK_Sale_Employee_EmployeeId foreign key references Employee(EmployeeId)	--销售人 外键Employee员工表EmployeeId员工id
)

create table SaleMingxi		--销售明细表
(
	SaleId int constraint FK_SaleMingxi_Sale_SaleId foreign key references Sale(SaleId),	--销售单编号 外键Sale销售表SaleId销售单编号
	ProductId int constraint FK_SaleMingxi_Product_ProductId foreign key references Product(ProductId),	--销售商品编号 外键Product商品目录表ProductId商品编号
	Number int,
	Price money,
	Discount int	--折扣
)

create table StockPile	--库存表
(
	StockPileId int primary key identity(1,1) not null,	--库存表编号
	DeptId int constraint FK_StockPile_Dept_DeptId foreign key references Dept(DeptId),	--商品所属部门
	StoreHouseId int constraint FK_StockPile_StoreHouse_StoreHouseId foreign key references StoreHouse(StoreHouseId),	--商品所在仓库 
	ProductId int constraint FK_StockPile_Product_ProductId foreign key references Product(ProductId),	--商品编号
	Number int
)

