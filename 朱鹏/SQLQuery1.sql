create database drp

go
drop database 
use drp
go

--进货表
create table Stock(

StockId int primary key identity(1,1),--商品编号
ItemName nvarchar(20) not null ,--物品名称
Quantity int not null default (0),--数量
Price money not null default (0.00), --单价
SumMoney money not null default (0.00),--总金额
StockTime  smalldatetime default (getdate())--进货时间

)

--存货表
create table Inventory(

InventoryId int primary key identity (1,1),
StockId int not null,--存货商品编号

InventoryTime smalldatetime default (getdate ())--入库时间
)


--出库表
create table StockRemoval3(
StockRemoval3 int primary key identity (1,1),
StockId int,
StockRemovalTime smalldatetime default (getdate())

)

--存取信息表
create table Access(
AccessId int primary key identity(1,1),
StockId int not null,
InventoryQuantity int not null,	--存入数量
StockRemovalQuantity int not null,--取出数量
AccessTime smalldatetime not null--时间
)


--销售表
create table Sell(
SellId int primary key identity (1,1),
InventoryId int not null,--销售商品
SellQuantity int not null,--销售数量
SellPrice money not null,--销售单价
SellSumMoney money not null,--销售总金额
SellTime smalldatetime default (getdate ())--销售时间


)

