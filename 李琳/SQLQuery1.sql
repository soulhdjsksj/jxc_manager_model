create database  Marke 
go

use Marke
go

create table StockInfo(--进货表
 StockId int primary key identity(1,1), --进货id
 GoodsNo varchar(20) not null,--商品编号
 SupplierName nvarchar(50) not null,--供应商名称
 Price float not null,--进货价格
 Number int not null,--进货数量
 TotalPrice float not null,--商品总价
 BuyDate datetime not null,--进货时间
)

create table GoodsInfo(--商品信息表  
  GoodsId int primary key identity(1,1),--商品id
  GoodsNo varchar(20)  not null , --商品编号
  SortId int not null,--商品类别id
  Goods nvarchar(50) not null ,--商品名称
  GoodsPrice float not null,--商品价格(进货的)
  GoodsPlace nvarchar(50) not null,--商品产地
  ProduceDate datetime not null,--生产日期
  ExpriationDate datetime not null,--保质期
)

create table InventoryInfo (--库存信息表
  InventoryId int primary key identity(1,1),--库存id
  GoodsNo varchar(20) not null, --商品编号
  GoodsCount int,--库存数量
  StockTime datetime not null,--入库时间
)

create table SellInfo (--商品销售表
  SellId int primary key identity(1,1),--销售id
  GoodsNo varchar(20) not null,--商品编号
  Price float not null,--出售单价
  Number int not null,--商品数量
  CommodityPrice float not null,--商品总价
  SellTime DateTime not null,--售出时间
)

create table SupplierInfo(--供应商信息表
  SupplierId int primary key identity(1,1),--供应商id
  SupplierName nvarchar(20) not null,--供应商名称
  SupplierLawyer nvarchar(20) not null,--法人代表
  SupplierPhone varchar(20) not null,--供应商电话
  SupplierAddress nvarchar(50) not null,--供应商地址
)

create table StaffInfo(--员工表
   StaffId int primary key identity(1,1),--员工id
   StaffName nvarchar(20) not null,--员工姓名
   SraffSex nvarchar(2) not null,--员工性别
   StaffPhone varchar(20) not null,--员工电话
)

create table Administrator(--管理员表
  AdministratorId int primary key identity(1,1),--管理员id
  AdministratorName nvarchar(20) not null,--管理员姓名
  AdministratorSex nvarchar(2) not null,--管理员性别
  AdministratorPhnoe varchar(20) not null,--管理员电话
) 

create table ClientInfo (--客户信息表
  ClientId int primary key identity(1,1),--客户id
  ClientName nvarchar(20) not null,--客户姓名
  ClientSex nvarchar(2) not null,--客户性别
  ClientPhone varchar(20) not null,--客户电话
  ClientAddress nvarchar(80) not null,--客户地址
 )

create table LoginInfo(--注册表
  UsersId int primary key identity(1,1),--用户id
  UsersName nvarchar(20) not null,--用户名
  UserPassword nvarchar(20) not null,--用户密码
)

