--进销存系统
create database InvoicingSystem
go

use InvoicingSystem


/*供货商表
供货id
商品id
供货商姓名
联系电话
供货商地址*/
create table Supplier
(
	SupplierId int primary key identity(1,1), --供货id
	CommodityId int not null,                 --商品id
	SupplierName nvarchar(50) not null,       --供货商姓名
	SupplierPhone  int not null,              --联系电话
	SupplierCustomerAddress nvarchar(100) not null, --供货商地址
)
go

/*仓库表
仓库Id
商品Id
仓库数量
分类id
入库时间 */
create table Warehouse
(
	WarehouseId int primary key identity(1,1), --仓库id
	CommodityId int  not null,				   --商品id
	WarehouseNumber int not null,			   --仓库数量
	ClassifyId int  not null,				   -- 分类id
	WarehouseTime smalldatetime not null,	   --入库时间
)

/*采购入库表
仓库id
商品Id
分类id
员工Id
采购数量
采购价钱
采购时间*/
create table EnterWarehouse
(
	 WarehouseId int  primary key  not null,     --仓库id
	 CommodityId int not null,				     --商品id
	 ClassifyId int  not null,				     -- 分类id
	 Employeeid int not null,				     --员工Id
	 EnterWarehouseNumber int not null,		     --采购数量
	 EnterWarehousePrice  money not null,		 --采购价钱
	 EnterWarehouseTime  smalldatetime not null, --采购时间
)

/*出库表
仓库id
商品id
分类id
员工Id
出库数量
销售id
出库时间*/
create table SetWarehouse
(
	WarehouseId int primary key not null,      --仓库id
	CommodityId int not null,			       --商品id
	ClassifyId int  not null,				   -- 分类id
	Employeeid int not null,			       --员工id
	SetWarehouseNumber int not null,   	       --出库数量
	SalesId int not null,					   --销售id
	SetWarehouseTime smalldatetime not null,   --出库时间
)
/*退库表
仓库Id
商品id
分类id
员工Id
退库数量
退库时间*/
create table RecedeWarehouse
(
	WarehouseId int primary key not null,      --仓库id
	CommodityId int not null,			       --商品id
	ClassifyId int  not null,				   -- 分类id
	Employeeid int not null,			       --员工id
	RecedeWarehouseNumber int not null,   	   --退库数量
	RecedeWarehouseTime smalldatetime not null,--退库时间
)


/*销售表
销售id
商品id
销售数量
客户id
销售价格
销售时间*/
create table Sales
(
	SalesId int primary key identity(1,1), --销售id
	CommodityId int not null,              --商品id
	SalesNumber int not null,			   --销售数量
	CustomerId  int not null,			   --客户id
	SalesPrice  money not null,			   --销售价格
	SalesTime smalldatetime not null,	   --销售时间
)


/*部门表
部门id
部门名称*/
create table  Department
(
	 DepartmentId int primary key identity(1,1),--部门id
	 DepartmentName nvarchar(80) not null,	    --部门名称
)

/*分类表
分类Id
分类Id*/
create table Classify
(
	ClassifyId int primary key identity(1,1),  --分类Id
	ClassifyName nvarchar(80) not null,        --分类名称

) 



/*员工表
员工id
员工姓名
员工性别
员工年龄
员工电话
员工地址
部门id*/
create table Employee
(
	 EmployeeId int primary key identity(1,1),	--员工Id
	 EmployeeName nvarchar(50) not null,        --员工姓名
	 EmployeeSex nvarchar(2) not null,			--员工性别
	 EmployeeAge int not null,					--员工年龄
	 EmployeePhone  int not null,               --员工电话
	 EmployeeAddress nvarchar(100) not null,    --员工地址
	 DepartmentId int not null,                 --部门id
)


/*商品表
商品id
仓库id
商品名称
商品地址
商品销售价格
商品生产日期*/
create table Commodity
(
	CommodityId int primary key identity(1,1), --商品id
	WarehouseId int  not null,                 --仓库id
	CommodityName nvarchar(80) not null,	   --商品名称
	CommodityAddress nvarchar(100) not null,   --商品地址
	CommodityPrice  money not null,			   --商品销售价格
	CommodityTime smalldatetime not null,      --商品生产日期
)



/*客户信息表
客户Id
客户名字
客户性别
客户年龄
客户电话
*/
create table  Customer
(
	CustomerId int primary key identity(1,1), --客户Id
	CustomerName nvarchar(50) not null,       --客户名字
	CustomerSex  nvarchar(2) not null,        --客户性别
	CustomerAge int not null,				  --客户年龄
	CustomerPhone  int not null,			  --客户电话
)


/*客户地址表
地址Id
客户Id
省份
市
详细地址*/
create table CustomerAddress
(
	CustomerAddressId int primary key identity(1,1),	 --地址Id
	CustomerId int not null,						     --客户Id
	Province nvarchar(20) not null,                      --省份
	City nvarchar(20) not null,							 --市
	DetailedAddress nvarchar(100) not null,				 --详细地址
)







