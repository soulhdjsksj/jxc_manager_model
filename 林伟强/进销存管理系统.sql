create database InvoicingSystem;

create table BasicInfo -- 基本信息
(
	Enterprise varchar(30) not null, --	企业
	Department int not null, -- 部门
	EmpNo int not null primary key, -- 员工编号
	EName varchar(10) not null, -- 员工姓名
	EmpSex varchar(2) not null, -- 员工性别
	WarAddress varchar(50) not null, -- 仓库地址
);
go
-- 货物采购
 -- 新增采购订单 New purchase Order
 create table NewpurchaseOrder
 (
	EmpNo int not null primary key, -- 员工编号
	NewPurCommodity varchar(50) not null, -- 新增采购商品
	NewCommodityType varchar(20) not null, -- 新增商品类型 
	PurchasingUnit varchar(30) not null, -- 采购单位
	ProStatus varchar(10) default (1) not null, -- 状态
	ProAddress varchar(30)  not null , -- 收货地址
	NewPurTime smalldatetime not null, -- 订单生成时间
 );
 go


-- 采购退货

-- 商品表Commodity List
create table CommodityList 
(
	ProId int not null, -- 商品编号
	ProName varchar(30) not null, -- 商品名称
	ProStorageRackNum varchar(30) not null, -- 商品货架
	ProInvento int default(0) not null, -- 商品存货
	ProStockout int default (0) not null, -- 商品缺货
	ProSalesVolume int default(0) not null, -- 销售量

)

-- 库存管理(入库，出库)

-- 创建入库表
create table InventoryTable
(
	InProID int not null, -- 入库商品编号
	Inspector varchar(20) not null, -- 验货员
	InProTime smalldatetime not null , -- 入库时间
);
-- 创建出库表
create table OutboundTable
(
	OutProID int not null, -- 出库商品编号
	Inspector varchar(20) not null, -- 验货员
	OutProTime smalldatetime not null, -- 出库时间
);
go