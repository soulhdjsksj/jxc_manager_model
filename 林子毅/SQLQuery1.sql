CREATE DATABASE Invoicing_System
USE Invoicing_System
GO
if exists (select 1
            from  sysobjects
           where  id = object_id('CommodityInfo')
            and   type = 'U')
   drop table CommodityInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CustomerInfo')
            and   type = 'U')
   drop table CustomerInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Inventory')
            and   type = 'U')
   drop table Inventory
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Permission')
            and   type = 'U')
   drop table Permission
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ProductCategory')
            and   type = 'U')
   drop table ProductCategory
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PurchaseList')
            and   type = 'U')
   drop table PurchaseList
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PurchasingOrder')
            and   type = 'U')
   drop table PurchasingOrder
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Role')
            and   type = 'U')
   drop table Role
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RolePermission')
            and   type = 'U')
   drop table RolePermission
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Sales')
            and   type = 'U')
   drop table Sales
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SalesOrder')
            and   type = 'U')
   drop table SalesOrder
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Salesman')
            and   type = 'U')
   drop table Salesman
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SupplierInfo')
            and   type = 'U')
   drop table SupplierInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCategory')
            and   type = 'U')
   drop table VipCategory
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Warehouse')
            and   type = 'U')
   drop table Warehouse
go

/*==============================================================*/
/* Table: CommodityInfo                                         */
/*==============================================================*/
create table CommodityInfo (
   Commodity_ID         int                  not null,
   Commodity_Name       varchar(50)          not null,
   Commodity_Barcode    varchar(50)          not null,
   Commodity_Class_No   int                  null,
   Treasury_Price       decimal              null,
   Distribution_Price   decimal              null,
   Retail_Price         decimal              null,
   Manufacturer         varchar(50)          null,
   Date_Updated         varchar(50)          null,
   Commodity_Postscript text                 null,
   constraint PK_COMMODITYINFO primary key (Commodity_ID)
)
go

/*==============================================================*/
/* Table: CustomerInfo                                          */
/*==============================================================*/
create table CustomerInfo (
   Vip_ID               int                  not null,
   Vip_Name             varchar(50)          null,
   Address              varchar(100)         null,
   Postcode             varchar(20)          null,
   Phone                varchar(20)          null,
   Company_Homepage     varchar(20)          null,
   Contatct             varchar(20)          null,
   "Comtact_E-mail"     varchar(50)          null,
   Vip_Category         int                  null,
   Cumulative_Consumption decimal              null,
   CustomerInfo_Postscript text                 null,
   constraint PK_CUSTOMERINFO primary key (Vip_ID)
)
go

/*==============================================================*/
/* Table: Inventory                                             */
/*==============================================================*/
create table Inventory (
   Inventory_ID         int                  not null,
   Date_Updated         varchar(50)          not null,
   Warehouse_No         varchar(50)          null,
   Commodity_ID         int                  null,
   Quantity             decimal              null,
   TotalMoney           decimal              null,
   Shortage_Warning     tinyint              null,
   Inventory_Postscript text                 null,
   constraint PK_INVENTORY primary key (Inventory_ID)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('Inventory')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Shortage_Warning')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'Inventory', 'column', 'Shortage_Warning'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '0正常，1警告',
   'user', @CurrentUser, 'table', 'Inventory', 'column', 'Shortage_Warning'
go

/*==============================================================*/
/* Table: Permission                                            */
/*==============================================================*/
create table Permission (
   Permission_ID        int                  not null,
   Permission_Name      varchar(50)          null,
   Permission_To        varchar(100)         null,
   Permission_Postscript text                 null,
   constraint PK_PERMISSION primary key (Permission_ID)
)
go

/*==============================================================*/
/* Table: ProductCategory                                       */
/*==============================================================*/
create table ProductCategory (
   Product_ID           int                  not null,
   Product_Name         varchar(50)          not null,
   Product_Postscript   text                 null,
   constraint PK_PRODUCTCATEGORY primary key (Product_ID)
)
go

/*==============================================================*/
/* Table: PurchaseList                                          */
/*==============================================================*/
create table PurchaseList (
   Purchase_Date        varchar(50)          not null,
   Salesman             varchar(50)          null,
   Purchase_ID          varchar(50)          not null,
   PurchaseOder_ID      varchar(50)          null,
   Supplier_ID          int                  null,
   Warehose_ID          varchar(50)          null,
   Commodity_ID         int                  null,
   Quantity             decimal              null,
   UnitPrice            decimal              null,
   Discount             decimal              null,
   TotalMoney           decimal              null,
   WhetherPayDown       varchar(10)          null,
   WhetherTally         varchar(10)          null,
   PrepaidAmount        decimal              null,
   PurchaseList_Postscript text                 null,
   constraint PK_PURCHASELIST primary key (Purchase_ID)
)
go

/*==============================================================*/
/* Table: PurchasingOrder                                       */
/*==============================================================*/
create table PurchasingOrder (
   OderData             varchar(50)          not null,
   Salesman             int                  null,
   PurchaseOder_ID      varchar(50)          not null,
   Supplier_ID          int                  null,
   Commodity_ID         int                  null,
   Quantity             decimal              null,
   UnitPrice            decimal              null,
   Discount             decimal              null,
   TotalMoney           decimal              null,
   State                tinyint              null,
   PurchasingOrder_Postscript text                 null,
   constraint PK_PURCHASINGORDER primary key (PurchaseOder_ID)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurchasingOrder')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurchasingOrder', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '0未确认，1确认，2退货，3确认并入库',
   'user', @CurrentUser, 'table', 'PurchasingOrder', 'column', 'State'
go

/*==============================================================*/
/* Table: Role                                                  */
/*==============================================================*/
create table Role (
   Role_ID              int                  not null,
   Role_Name            varchar(50)          not null,
   Role_Postscript      text                 null,
   constraint PK_ROLE primary key (Role_ID)
)
go

/*==============================================================*/
/* Table: RolePermission                                        */
/*==============================================================*/
create table RolePermission (
   RolePermission_ID    int                  not null,
   Permission_ID        varchar(50)          null,
   Role_ID              varchar(50)          null,
   constraint PK_ROLEPERMISSION primary key (RolePermission_ID)
)
go

/*==============================================================*/
/* Table: Sales                                                 */
/*==============================================================*/
create table Sales (
   Sales_Data           varchar(50)          not null,
   Salesman             varchar(50)          null,
   Sales_ID             varchar(50)          not null,
   SalesOrder_ID        varchar(50)          null,
   Customer_ID          int                  null,
   Warehouse_No         varchar(50)          null,
   Commodity_ID         int                  null,
   Quantity             decimal              null,
   UnitPrice            decimal              null,
   Discount             decimal              null,
   TotalMoney           decimal              null,
   hetherPayDown        varchar(10)          null,
   WhetherTally         varchar(10)          null,
   PrepaidAmount        decimal              null,
   Sales_Postscript     text                 null,
   constraint PK_SALES primary key (Sales_ID)
)
go

/*==============================================================*/
/* Table: SalesOrder                                            */
/*==============================================================*/
create table SalesOrder (
   SalesOrder_Dare      varchar(50)          not null,
   Salesman             int                  null,
   SalesOrder_ID        varchar(50)          not null,
   Customer_ID          int                  null,
   Commodity_ID         int                  null,
   Quantity             decimal              null,
   UnitPrice            decimal              null,
   Discount             decimal              null,
   TotalMoney           decimal              null,
   State                tinyint              null,
   SalesOrder_Postscript text                 null,
   constraint PK_SALESORDER primary key (SalesOrder_ID)
)
go

/*==============================================================*/
/* Table: Salesman                                              */
/*==============================================================*/
create table Salesman (
   Salesman_ID          int                  not null,
   Salesman_Name        varchar(50)          not null,
   Login_Name           varchar(50)          not null,
   Login_Password       varchar(50)          not null,
   Phone                varchar(50)          null,
   Home_Address         varchar(100)         null,
   ID_Card_No           varchar(50)          null,
   Role_ID              int                  not null,
   Salesman_Postscript  text                 null,
   constraint PK_SALESMAN primary key (Salesman_ID)
)
go

/*==============================================================*/
/* Table: SupplierInfo                                          */
/*==============================================================*/
create table SupplierInfo (
   Supplier_ID          int                  not null,
   Supplier_Name        varchar(50)          not null,
   Address              varchar(100)         null,
   Postcode             varchar(20)          null,
   Phone                varchar(20)          null,
   Company_Homepage     varchar(50)          null,
   "Contact_E-mail"     varchar(50)          null,
   Contact              varchar(20)          null,
   SupplierInfo_Postscript text                 null,
   constraint PK_SUPPLIERINFO primary key (Supplier_ID)
)
go

/*==============================================================*/
/* Table: VipCategory                                           */
/*==============================================================*/
create table VipCategory (
   VipCategory_ID       int                  not null,
   VipCategory_Name     varchar(50)          null,
   VipCategory_Postscript text                 null,
   constraint PK_VIPCATEGORY primary key (VipCategory_ID)
)
go

/*==============================================================*/
/* Table: Warehouse                                             */
/*==============================================================*/
create table Warehouse (
   Warehouse_No         varchar(50)          not null,
   Warehouse_Name       varchar(50)          not null,
   Warehouse_Postscript text                 null,
   constraint PK_WAREHOUSE primary key (Warehouse_No)
)
go
