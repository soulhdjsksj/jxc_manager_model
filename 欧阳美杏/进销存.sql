create database Bank;
go

use Bank;
go
-- 部门表
create table Department(
    DepartmentId int primary key identity(1,1) ,--部门编号
	DepartmentName nvarchar(20) not null, --部门名称
);

--员工表
create table AccountInfo(
   AccountId int primary key identity(1,1), --员工编号
   RealName nvarchar(20) not null, --员工姓名
   Sex nvarchar(2) not null, --员工性别
   DepartmentId int not null, --部门编号
   AccountPhone varchar not null, --员工联系方式
)

--供应商表
create table Distributor(
   DistributorId int primary key identity(1,1), --供应商编号
   DistributorName nvarchar(20) not null, --供应商名称
   Location nvarchar(80) not null, --地址
   DisributorPhone varchar(20) not null, -- 供应商联系方式
   SupplierContact nvarchar(20) not null, -- 联系人

)

--商品表
create table Product(
   ProductId int primary key identity(1,1), --商品编号
   ProductName nvarchar(30) not null, --商品名称
   ProductNum nvarchar(20) not null, --商品数量
   ProductDate varchar(30) not null, --生产日期
   ExpirationDate varchar(20) not null, --保质期
   
)




--仓库表
create table Warehouse(
   WarehouseId int primary key identity(1,1), --仓库编号
   WarehouseLoca nvarchar(20) not null, --仓库地址
   WarehousePhone varchar(20) not null, --仓库电话
   AccountId int not null, --员工编号

)

--进库表
create table Stock(
  StockId int primary key identity(1,1), --进库编号
  StockTime smalldatetime not null, --进库时间
  ProductName nvarchar(20) not null, --商品名称
  ProductNum nvarchar(20) not null, --商名数量
  ProductId int not null, --商品编号
)


--出库表
create table StockRemoval(
  StockRemovalId int primary key identity(1,1), --出库编号
  StockRemovalTime smalldatetime not null, --出库时间
  AccountId int not null, --员工编号
  ProductId int not null, --商品编号
  StockRemovalNum nvarchar(20) not null, --出库数量
)

--库存表
create table Inventory(
  InventoryId int primary key identity, --库存编号
  ProductId varchar(20) not null, --商品编号
  StockTime smalldatetime not null, --进库时间
  StockRemovalTime smalldatetime not null, --出库时间
  RemainProduct nvarchar(20), --所存商品
)


--客户表
create table Correspondent(	
 CorrespondentId int primary key identity,--客户编号
 CorrespondentName nvarchar(20) not null, --客户名称
 CorrespondentPhone varchar(30) not null, --客户联系电话
 CorrespondentLoca nvarchar(80) not null, --客户地址 

)


--注册表

create table LoginInfo(
   LoginId int primary key identity(1,1), --用户编号
   LoginNum nvarchar(16) not null, --注册名称
   LoginPassword nvarchar(20) not null, --注册密码
   LoginMobile varchar(11) not null, --电话号码
)