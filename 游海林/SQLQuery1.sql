

create table Users   /*用户表*/  
(  
  User_Id     varchar(10) ,  
  User_Pwd    varchar(10)  ,  
  Again_Pwd   varchar(10)  ,  
  Bel_Group   varchar(10)  ,  
  Div_Type    varchar(10)  ,  
  User_Auth   varchar(10) ,  
  Auth_Type   varchar(10)  ,  
  User_Status varchar(10)  ,  
  Create_User varchar(10)  ,  
  Create_Date varchar(10)  ,  
  Create_Time varchar(10)  ,  
  Appr_User   varchar(10)  ,  
  Appr_Date   varchar(10)  ,  
  Appr_Time   varchar(10)  ,  
  Pwd_Date    varchar(10)  ,  
  Err_Count   float  ,  
  Use_eJCIC   varchar(10)    
) 
 
CREATE TABLE Supplier  /*供应商表*/  
(  
  Supplier_ID     int     IDENTITY(1,1)     NOT NULL, /* 供应商编号 ,主键 */  
  Name            varchar(250)              NOT NULL, /* 供应商名称 */  
  Address         varchar(250)              NOT NULL, /* 地址 */  
  Phone           varchar(25)                       ,     /* 电话 */  
  Fax             varchar(25) ,                       /* 传真 */  
  PostalCode      varchar(10) ,                       /* 邮编 */  
  ConstactPerson  varchar(20)                         /* 联系人 */  
 )  
  
CREATE TABLE Customer   /* 客户表*/  
(  
  Customer_ID     int    IDENTITY(1,1)      NOT NULL, /* 客户编号,主键*/  
  Name            varchar(20)              NOT NULL, /* 客户名称 */  
  Address         varchar(20)              NOT NULL, /* 地址 */   
  Phone           varchar(20)  ,     /* 电话 */  
  Fax             varchar(20) ,     /* 传真 */  
  PostalCode      varchar(20) ,     /* 邮编 */  
  ConstactPerson  varchar(20)       /* 联系人 */  
 )   
  
CREATE TABLE Dept      /*部门表*/  
(  
  Dept_ID        int   IDENTITY(1,1)        NOT NULL, /* 部门编号,主键 */  
  Name           varchar(30)                NOT NULL, /* 名称 */  
  Remark           varchar(80)               NOT NULL/* 描述,备注 */   
)  
  
CREATE TABLE Dept_Supplier /*部门--供应商表*/  
(  
  Dept_ID       int                         NOT NULL,  /* 部门编号 */  
  Supplier_ID   int                         NOT NULL   /* 供应商编号   */  
)  
  
CREATE TABLE Dept_Customer /*部门--客户表*/  
(  
  Dept_ID       int                         NOT NULL, /* 部门编号 */  
  Customer_ID   int                         NOT NULL  /* 客户编号  */  
)  
  
CREATE TABLE StoreHouse   /* 仓库表 */  
(  
  StoreHouse_ID   int IDENTITY(1,1)         NOT NULL,  /* 仓库编号,  */  
  Address         varchar(80)              NOT NULL,  /* 地址 */  
  Phone           varchar(20) ,      /* 电话 */  
  Employee_ID     INT                       NOT NULL,  /* 仓库保管  */  
  CreateDate      datetime         /* 仓库成立时间 */  
)  
  
CREATE TABLE ProductClass  /* 商品总分类表 */  
(  
  ProductClass_ID  int IDENTITY(1,1)        NOT NULL,  /* 商品总分类编号,   */   
  Name             varchar(30)              NOT NULL,  /* 商品分类名称 */  
  Employee_ID      INT                      NOT NULL,  /* 建分类人 ,外键  */  
  CreateDate       datetime  ,      /* 建分类时间 */  
  Remark           varchar(80) ,    /* 描述,备注 */  
)  
  
CREATE TABLE ProductList  /* 商品细分类表 */  
(  
  ProductClass_ID  INT                      NOT NULL, /* 商品总分类编号  */  
  ProductList_ID   int IDENTITY(1,1)        NOT NULL, /* 商品细分类编号   */  
  Name             varchar(30)              NOT NULL, /* 商品名称 */  
  Employee_ID      INT                      NOT NULL, /* 建分类人  ( 参照 EMPLOYEE 表 )*/  
  CreateDate       datetime   ,     /* 建分类时间 */  
  Remark           varchar(80) ,   /* 描述 ,备注 */  
 )  
  
CREATE TABLE ProductSpec  /* 商品规格表 */  
(  
  ProductSpec_ID   INT IDENTITY(1,1)        NOT NULL, /* 商品规格编号  */  
  Name             varchar(30)              NOT NULL, /* 商品规格名称 */  
  Employee_ID      INT                      NOT NULL, /* 操作员 */  
  CreateDate       datetime ,     /* 创建时间 */  
  Remark           varchar(80)   /* 描述,备注 */  
)
  
CREATE TABLE ProductUnit /* 商品计量单位表 */  
(  
  ProductUnit_ID   INT IDENTITY(1,1)        NOT NULL, /* 计量单位编号   */  
  Name             varchar(30)              NOT NULL, /* 计量单位名称 */  
  Employee_ID      INT                      NOT NULL, /* 操作员  */  
  CreateDate       datetime  ,     /* 创建时间 */  
  Remark           varchar(80)        /* 描述,备注 */  
)  
   
CREATE TABLE Product    /* 商品目录表 */  
(  
  ProductList_ID   int                      NOT NULL,  /* 商品细分类编号  */  
  Product_ID       INT IDENTITY(1,1)        NOT NULL,  /* 商品名称编号  */  
  Name             varchar(30)              NOT NULL,  /* 商品名称 */  
  ProductSpec_ID   INT                      NOT NULL,  /* 商品规格 */  
  ProductUnit_ID   INT                      NOT NULL,  /* 计量单位 */  
  Price            MONEY   ,      /* 参考价格 */  
  Employee_ID      INT                      NOT NULL,  /* 操作员 */  
  CreateDate       datetime   ,      /* 创建时间 */  
  Remark             varchar(80)      /* 描述,备注 */  
)  
  
CREATE TABLE Product_Supplier  /* 商品--供应商表 */  
(   
  Product_ID       INT                      NOT NULL,   /* 商品名称编号 */  
  Supplier_ID      INT                      NOT NULL    /* 供应商编号 */  
)  
  
CREATE TABLE Employee         /* 员工表 */  
(   
  Employee_ID      INT IDENTITY(1,1)        NOT NULL,  /* 员工编号 */  
  Dept_ID          INT                      NOT NULL,  /* 所属部门编号 */  
  Name             varchar(30)              NOT NULL,  /* 姓名 */  
  Duty             varchar(20)              NOT NULL,  /* 职务 */  
  Gender           varchar(6)               NOT NULL,  /* 性别 */  
  BirthDate        datetime                 NOT NULL,  /* 出生日期 */  
  HireDate         datetime    ,      /* 合同签订 日期 */  
  MatureDate       datetime  ,      /* 合同到期日 */  
  IdentityCard     varchar(20)  ,      /* 身份证号 */  
  Address          varchar(80)  ,      /* 住址 */  
  Phone            varchar(25)  ,      /* 电话 */  
  Email            varchar(30)           /* E_MAIL */  
)  
   
CREATE TABLE BuyOrder         /* 进货合同 */  
(  
  BuyOrder_ID     INT IDENTITY(1,1)        NOT NULL, /* 进货合同编号  */  
  WriteDate       datetime                 NOT NULL, /* 合同签订日期  */  
  InsureDate      datetime                 NOT NULL, /* 合同生效日期  */  
  EndDate         datetime                 NOT NULL, /* 合同到期日期  */  
  Dept_ID         INT                      NOT NULL, /* 签订部门 */  
  Supplier_ID     INT                      NOT NULL, /* 供应商 */  
  Employee_ID     INT                      NOT NULL  /* 合同主要负责人  */  
)  

CREATE TABLE BuyOrder_Detail  /* 进货合同明细表 */  
(  
  BuyOrder_ID     INT                      NOT NULL,  /* 进货合同编号  */  
  Product_ID      INT                      NOT NULL,  /* 所进商品编号  */   
  Quantity        INT                      NOT NULL,  /* 商品数量 */  
  Price           money                    NOT NULL   /* 商品进价 */  
)  
  
  
CREATE TABLE EnterStock    /* 入库单表 */  
(  
  EnterStock_ID    INT IDENTITY(1,1)       NOT NULL, /* 入库单编号  */  
  EnterDate        datetime                NOT NULL, /* 入库时间 */  
  Dept_ID          INT                     NOT NULL, /* 入库部门 */  
  StoreHouse_ID    INT                     NOT NULL, /* 所入仓库  */  
  Employee_ID      INT                     NOT NULL  /* 入库人*/  
  
)  
  
CREATE TABLE EnterStock_Detail /* 入库单明细 */  
(   
  EnterStock_ID    INT                     NOT NULL, /* 入库单编号  */  
  Product_ID       INT                     NOT NULL, /* 此种商品编号 */   
  Quantity         int                     NOT NULL, /* 此种商品数量 */  
  Price            money ,     /* 此种商品参考价格  */  
  HaveInvoice      bit                     not null, /* 此种商品有没有开发票  )*/  
  InvoiceNum       varchar(30)     /* 发票号 */  
)  
  
  
CREATE TABLE BackStock  /* 退库单表 */  
(  
  BackStock_ID     INT IDENTITY(1,1)       NOT NULL, /* 退库单编号  */  
  BackDate         datetime                NOT NULL, /* 退库时间 */  
  Dept_ID          INT                     NOT NULL, /* 退库部门  */  
  StoreHouse_ID    INT                     NOT NULL, /* 所退入仓库  */  
  Employee_ID      INT                     NOT NULL, /* 退库人 */  
  Remark           varchar(80)      /* 退库原因 */  
)  

CREATE TABLE BackStock_Detail /*退库单明细表*/  
(   
  BackStock_ID     INT                     NOT NULL, /* 退库单编号 */   
  Product_ID       INT                     NOT NULL, /* 所退商品编号  */   
  Quantity         int                     NOT NULL, /* 退入数量 */  
  Price            money       /* 参考价格 */  
)  
  
CREATE TABLE LeaveStock  /* 出库单表 */  
(  
  LeaveStock_ID    INT IDENTITY(1,1)       NOT NULL,  /* 出库单编号 */  
  LeaveDate        datetime                NOT NULL,  /* 出库时间 */   
  Dept_ID          INT                     NOT NULL,  /* 出库部门 */  
  StoreHouse_ID    INT                     NOT NULL,  /* 所出仓库  */  
  ToStoreHouse_ID  INT                     NOT NULL,  /* 所入仓库  */  
  Employee_ID      INT                     NOT NULL   /* 出库人 */  
   
)  
  
CREATE TABLE LeaveStock_Detail  /* 出库单明细表 */  
(   
  LeaveStock_ID    INT                     NOT NULL,  /* 出库单编号 */   
  Product_ID       INT                     NOT NULL,  /* 所出商品编号  */   
  Quantity         int                     NOT NULL,  /* 出库数量 */  
  Price            money        /* 出库价格 */   
)  
  
CREATE TABLE BackSale  /* 退货单表 */  
(  
  BackSale_ID      INT IDENTITY(1,1)       NOT NULL,  /* 退货单编号   */  
  BackDate         datetime                NOT NULL,  /* 退货日期  */   
  Dept_ID          INT                     NOT NULL,  /* 退货部门  */  
  StoreHouse_ID    INT                     NOT NULL,  /* 退入仓库  */  
  Employee_ID      INT                     NOT NULL,  /* 退货人  */  
  Remark           varchar(80)      /* 退货原因 */  
  
)  
CREATE TABLE BackSale_Detail  /*退货单明细表*/  
(   
  BackSale_ID      INT                     NOT NULL,  /* 退货单编号  */   
  Product_ID       INT                     NOT NULL,  /* 所退商品编号  */   
  Quantity         int                     NOT NULL,  /* 退货数量 */  
  Price            money      /* 价格 */   
)  
  
  
CREATE TABLE SaleOrder    /*销售合同*/  
(  
  SaleOrder_ID     INT IDENTITY(1,1)       NOT NULL,  /* 合同编号  */  
  WriteDate        datetime                NOT NULL,  /* 合同签订日期  */  
  InsureDate       datetime                NOT NULL,  /* 合同生效日期  */  
  EndDate          datetime                NOT NULL,  /* 合同到期日期  */  
  Dept_ID          INT                     NOT NULL,  /* 签订部门  */  
  Customer_ID      INT                     NOT NULL,  /* 客户编号  */  
  Employee_ID      INT                     NOT NULL   /* 合同主要负责人  */  
)  

CREATE TABLE SaleOrder_Detail  /* 销售合同明细表 */  
(  
  SaleOrder_ID     INT                     NOT NULL,  /* 销售合同编号  */  
  Product_ID       INT                     NOT NULL,  /* 销售商品编号  */   
  Quantity         int                     not null,  /* 商品数量 */  
  Price            money         /* 商品进价 */  
)  
  
  
CREATE TABLE Buy     /* 进货表(验货表)*/  
(  
   Buy_ID          INT IDENTITY(1,1)         NOT NULL, /* 进货编号   */  
   ComeDate        datetime                  NOT NULL, /* 进货日期 */  
   Dept_ID         INT                       NOT NULL, /* 进货部门  */   
   Employee_ID     INT                       NOT NULL  /* 验货人 */  
)  
  
CREATE TABLE Buy_Detail  /*进货表明细(验货表)*/    
(  
  Buy_ID           INT                      NOT NULL, /* 进货编号  */  
  Product_ID       INT                      NOT NULL, /* 商品编号 */   
  BuyOrder_ID      INT     ,     /* 采购合同  */  
  Quantity         int                      not null, /* 数量 */  
  Price            money           /* 价格 */  
      
)  
  
CREATE TABLE Sale   /* 销售表*/  
(  
  Sale_ID          INT IDENTITY(1,1)        NOT NULL,  /* 销售 编号  */  
  SaleDate         datetime                 not null,  /* 销售 日期 */  
  Dept_ID          INT                      NOT NULL,  /* 销售部门  */   
  Employee_ID      INT                      NOT NULL   /* 售货人 */  
)  
  
CREATE TABLE Sale_Detail  /* 销售明细 ( 验货表 ) */    
(  
  Sale_ID          INT                      NOT NULL,  /* 销售编号  */  
  Product_ID       INT                      NOT NULL,  /* 商品编号  */    
  SaleOrder_ID     INT   ,      /* 销售合同  */  
  Quantity         int                      not null,  /* 数量 */  
  Price            money                    not null,  /* 价格 */  
  Discount         int           /* 折扣 */  
  
)  
  
  
CREATE TABLE StockPile  /* 库存表 */  
(   
  StockPile_ID     INT IDENTITY(1,1)        NOT NULL, /* 库存编号 */   
  Dept_ID          INT                      NOT NULL, /* 商品所属部门 */    
  StoreHouse_ID    INT                      NOT NULL, /* 所在仓库  */     
  Product_ID       INT                      NOT NULL, /* 商品编号  */     
  FirstEnterDate   datetime                 not null, /* 此种商品第一次入库时间 */  
  LastLeaveDate    datetime    ,     /* 此种商品最后一次出库时间 */  
  Quantity         int                      not null, /* 所存数量 */  
  Price            money                    not null  /* 加权价 */  
    
)



  
