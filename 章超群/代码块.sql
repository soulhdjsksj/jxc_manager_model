create database Shopping
go

use Shopping
go


--用户信息表(用户Id,用户联系方式,用户地址,用户姓名)
create table AccountInfo
	(	
		AccountId int primary key  identity (1,1),
		AccountPhone varchar(20) not null,
		AccountAddress varchar(100) not null,
		RealName varchar(20) not null
	)
--货物信息表—订货信息 (仓库id 购买商品名称  商品种类   商品数量  商品销售价格 购买时间 收货地址)
create table Cargo
	(
		WarehouseId int not null,
		CommodityName varchar(20) not null,
		CommodityType varchar(20) not null,
		SellPrice money not null,
		PurchaseTime smalldatetime not null,
		ReceiveAddress varchar(100) not null
	)
--销售记录信息表—用户的每笔支出费用 类别（获得用户偏好进行推荐  用户id 购买商品 购买种类 购买价格 月销售量) 
	create table CommodityRecord 
	(
		AccountId int not null,
		CommodityId int not null,
		CommodityName varchar(20) not null,
		CommodityType varchar(20) not null,
		PurchasePrice smalldatetime not null,
		MonthSellAmount int not null
	)
--仓库表—记录所有货物信息(货物Id方便查找,货物名称,货物种类  商品成本价格  货物存数量)
	create table Warehouse
	(
	WarehouseId int primary key identity (1,1),
	WarehouseName varchar(20) not null,
	WarehouseType varchar(20) not null,
	CommodityCostPrice money not null,
	WarehouseAmount int not null

	)
--库存管理表-记录仓库数据（针对仓库表记录货物信息:  货物id  货物调用时间  货物调用状态（分存储和减少） 货物数量）
	create table WarehouseRecord
	(
		WarehouseId int not null,
		UseTime smalldatetime not null,
		UseState varchar(20) not null,
		WarehouseAmount int not null
	)
--供应商表—记录其的联系方式 地址 货物类别 相关信息(仓库id  供应商id  姓名联系方式 货物仓库地址 )
	create table Provider
	(
		ProviderId int primary key identity (1,1),
		ProviderPhone varchar(30) not null,
		WarehouseId int not null,
		ProviderAddress varchar(60) not null
	) 
--供应商销售记录表—记录销售记录销量（获得供应商的销售信息 来评判信誉排行以及货物热度等 供应商id 销售商品  种类 月销售量 成交金额）
	create table ProviderRecord(
		ProviderId int not null,
		SellCommodity varchar(20) not null,
		CommodityType varchar(30) not null,
		MonthSellAmount int not null,
		MonthSumSellMoney int not null                            
	)



 --PowerDesigner 代码块
if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('AccountInfo') and o.name = 'FK_ACCOUNTI_REFERENCE_WAREHOUS')
alter table AccountInfo
   drop constraint FK_ACCOUNTI_REFERENCE_WAREHOUS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Cargo') and o.name = 'FK_CARGO_REFERENCE_COMMODIT')
alter table Cargo
   drop constraint FK_CARGO_REFERENCE_COMMODIT
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CommodityRecord') and o.name = 'FK_COMMODIT_REFERENCE_ACCOUNTI')
alter table CommodityRecord
   drop constraint FK_COMMODIT_REFERENCE_ACCOUNTI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Provider') and o.name = 'FK_PROVIDER_REFERENCE_WAREHOUS')
alter table Provider
   drop constraint FK_PROVIDER_REFERENCE_WAREHOUS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ProviderRecord') and o.name = 'FK_PROVIDER_REFERENCE_PROVIDER')
alter table ProviderRecord
   drop constraint FK_PROVIDER_REFERENCE_PROVIDER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WarehouseRecord') and o.name = 'FK_WAREHOUS_REFERENCE_WAREHOUS')
alter table WarehouseRecord
   drop constraint FK_WAREHOUS_REFERENCE_WAREHOUS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AccountInfo')
            and   type = 'U')
   drop table AccountInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Cargo')
            and   type = 'U')
   drop table Cargo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CommodityRecord')
            and   type = 'U')
   drop table CommodityRecord
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Provider')
            and   type = 'U')
   drop table Provider
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ProviderRecord')
            and   type = 'U')
   drop table ProviderRecord
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Warehouse')
            and   name  = 'Unique_Wareshop_WareshopId'
            and   indid > 0
            and   indid < 255)
   drop index Warehouse.Unique_Wareshop_WareshopId
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Warehouse')
            and   type = 'U')
   drop table Warehouse
go

if exists (select 1
            from  sysobjects
           where  id = object_id('WarehouseRecord')
            and   type = 'U')
   drop table WarehouseRecord
go

/*==============================================================*/
/* Table: AccountInfo                                           */
/*==============================================================*/
create table AccountInfo (
   AccountId            int                  identity(1,1),
   WarehouseId          int                  null,
   AccountPhone         varchar(20)          not null,
   AccountAddress       varchar(100)         not null,
   RealName             varchar(20)          not null,
   constraint PK_ACCOUNTINFO primary key (AccountId)
)
go

/*==============================================================*/
/* Table: Cargo                                                 */
/*==============================================================*/
create table Cargo (
   WarehouseId          int                  identity(1,1),
   AccountId            int                  identity(1,1),
   CommodityName        varchar(20)          not null,
   CommodityType        varchar(20)          not null,
   SellPrice            money                not null,
   PurchaseTime         datetime             not null,
   ReceiveAddress       varchar(20)          not null,
   constraint PK_CARGO primary key (AccountId)
)
go

/*==============================================================*/
/* Table: CommodityRecord                                       */
/*==============================================================*/
create table CommodityRecord (
   AccountId            int                  not null,
   CommodityId          int                  not null,
   CommodityName        varchar(20)          not null,
   CommodityType        varchar(20)          not null,
   PurchasePrice        int                  not null,
   MonthSellAmount      int                  not null,
   constraint PK_COMMODITYRECORD primary key (AccountId)
)
go

/*==============================================================*/
/* Table: Provider                                              */
/*==============================================================*/
create table Provider (
   ProviderId           int                  not null,
   ProviderPhone        varchar(20)          not null,
   ProviderAddress      varchar(20)          not null,
   WarehouseId          int                  null,
   constraint PK_PROVIDER primary key (ProviderId)
)
go

/*==============================================================*/
/* Table: ProviderRecord                                        */
/*==============================================================*/
create table ProviderRecord (
   ProviderId           int                  null,
   SellCommodity        varchar(20)          null,
   CommodityType        varchar(20)          null,
   MonthSellAmount      int                  null,
   MonthSumSellMoney    money                null
)
go

/*==============================================================*/
/* Table: Warehouse                                             */
/*==============================================================*/
create table Warehouse (
   WarehouseId          int                  identity(1,1),
   WarehouseName        varchar(20)          not null,
   WarehouseType        varchar(20)          not null,
   CommodityCostPrice   money                not null
      constraint CKC_COMMODITYCOSTPRIC_WAREHOUS check (CommodityCostPrice >= 0),
   WarehouseAmount      int                  not null,
   constraint PK_WAREHOUSE primary key (WarehouseId)
)
go

/*==============================================================*/
/* Index: Unique_Wareshop_WareshopId                            */
/*==============================================================*/
create unique index Unique_Wareshop_WareshopId on Warehouse (
WarehouseId ASC
)
go

/*==============================================================*/
/* Table: WarehouseRecord                                       */
/*==============================================================*/
create table WarehouseRecord (
   WarehouseId          int                  not null,
   UseTime              datetime             not null,
   货物使用状态               varchar(20)          not null,
   WarehouseAmount      int                  not null,
   constraint PK_WAREHOUSERECORD primary key (WarehouseId)
)
go

alter table AccountInfo
   add constraint FK_ACCOUNTI_REFERENCE_WAREHOUS foreign key (WarehouseId)
      references Warehouse (WarehouseId)
go

alter table Cargo
   add constraint FK_CARGO_REFERENCE_COMMODIT foreign key (AccountId)
      references CommodityRecord (AccountId)
go

alter table CommodityRecord
   add constraint FK_COMMODIT_REFERENCE_ACCOUNTI foreign key (AccountId)
      references AccountInfo (AccountId)
go

alter table Provider
   add constraint FK_PROVIDER_REFERENCE_WAREHOUS foreign key (ProviderId)
      references Warehouse (WarehouseId)
go

alter table ProviderRecord
   add constraint FK_PROVIDER_REFERENCE_PROVIDER foreign key (ProviderId)
      references Provider (ProviderId)
go

alter table WarehouseRecord
   add constraint FK_WAREHOUS_REFERENCE_WAREHOUS foreign key (WarehouseId)
      references Warehouse (WarehouseId)
go



--库存管理表
--仓库表
--客户表
--货物信息表
--销售记录信息表
--供应商表
--供应商销售记录表

 