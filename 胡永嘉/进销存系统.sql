create database InvoicingSystem
go
use  InvoicingSystem
--------------------------------------------------------------------------------------------
create table NameTable---员工姓名表
(
NameID int primary key identity(1,1), --员工ID
Name nvarchar(10) not null,--员工名字
PhoneNumber varchar(20) not null --员工电话
)

--------------------------------------------------------------------------------------------
create table GoodsMessage ---货品信息表
(
GoodsID int primary key identity(1,1),--货品ID
GoodsName nvarchar(30) not null, --货品名称
GoodsVende nvarchar(10) not null, --货品供货商
GoodsExpirationDate smalldatetime not null --货品保质期
) 
------------------------------------------------------------------------------------------
create table PurRqs ---采购申请表
(
 PurRqsID int primary key identity(1,1), --申请ID,
 PurRqsNameID int not null ,--申请人ID
 GoodsID int not null ,--货品ID
 GoodsMoney money not null , -- 货品价格
 GoodsNumber int not null, -- 货品数量
 GoodsRemark nvarchar(50) default('无'), --货品备注
 PurRqsTime smalldatetime not null --进货时间
)

--采购申请表与货品信息表的货品ID主外键关联
alter table PurRqs
add constraint FK_GoodsMessageGoodsID_PurRqsGoodsID foreign key(GoodsID) references GoodsMessage(GoodsID)

--申请人ID与员工姓名表主外键关联
alter table PurRqs
 add constraint FK_NameTableNameID_PurRqsPurRqsNameID foreign key(PurRqsNameID) references NameTable(NameID)
----------------------------------------------------------------------------------------------
create table OrderGoods  ---订货表
(
 OrderGoodsID int primary key identity(1,1),--订货ID
 OrderGoodsNameID int not null,--订货人ID
 GoodsID int not null , --货品ID
 GoodsMoney money not null ,--货品价格
 GoodsNumder int not null, --货品数量
 OrderGoodsTime smalldatetime not null --订货时间
)

--订货表与货品信息表的货品ID主外键关联
alter table OrderGoods
add constraint FK_OrderGoodsGoodsID_PurRqsGoodsID foreign key(GoodsID) references GoodsMessage(GoodsID)

--订货人ID与员工姓名主外键关联
 alter table OrderGoods
 add constraint FK_NameTableNameID_OrderGoodsOrderGoodsNameID foreign key(OrderGoodsNameID) references NameTable(NameID)
------------------------------------------------------------------------------------------
create table Repertory -- 库存表
(
RepertoryID int primary key identity(1,1), --库存ID
RepertoryNameID int not null , --库存货品ID
RepertoryNumder int not null , --库存数量
RepertoryInTime smalldatetime not null --入库时间
)

--库存表与货品信息表的货品ID主外键关联
alter table Repertory
add constraint FK_RepertoryID_PurRqsGoodsID foreign key(RepertoryNameID) references GoodsMessage(GoodsID)
-----------------------------------------------------------------------------------------
create table RepertoryInDeal -- 库存入表
(
 RepertoryInDealID int primary key identity(1,1), --入货交易ID
 InRepertoryGoodsID int not null , --入库货品ID
 Numder int not null , --数量
 RepertoryDealTime smalldatetime not null --库存出时间
)

--库存入表与货品信息表的货品ID主外键关联
alter table RepertoryInDeal
add constraint FK_InRepertoryGoodsID_PurRqsGoodsID foreign key(InRepertoryGoodsID) references GoodsMessage(GoodsID)

--------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
create table RepertoryOutDeal -- 库存出表
(
 RepertoryOutDealID int primary key identity(1,1), --出货交易ID
 OutRepertoryGoodsID int not null , --出库货品ID
 Numder int not null , --数量
 RepertoryDealTime smalldatetime not null --库存出时间
)

--库存入表与货品信息表的货品ID主外键关联
alter table RepertoryOutDeal
add constraint FK_OutRepertoryGoodsID_RepertoryNameID foreign key(OutRepertoryGoodsID) references GoodsMessage(GoodsID)
--------------------------------------------------------------------------------------------

----------------------------------------------------------------

----------------------------------------------------------------
---管理员数据
select * from NameTable
insert into NameTable(Name,PhoneNumber) values ('小胖','1596637558')
insert into NameTable(Name,PhoneNumber) values ('刘备','1596637558')
insert into NameTable(Name,PhoneNumber) values ('赵云','1596637558')
insert into NameTable(Name,PhoneNumber) values ('马超','1596637558')
insert into NameTable(Name,PhoneNumber) values ('关羽','1596637558')
insert into NameTable(Name,PhoneNumber) values ('张飞','1596637558')
insert into NameTable(Name,PhoneNumber) values ('黄忠','1596637558')
insert into NameTable(Name,PhoneNumber) values ('诸葛亮','1596637558')
insert into NameTable(Name,PhoneNumber) values ('曹操','1596637558')
insert into NameTable(Name,PhoneNumber) values ('白起','1596637558')
insert into NameTable(Name,PhoneNumber) values ('小乔','1596637558')
insert into NameTable(Name,PhoneNumber) values ('大乔','1596637558')
insert into NameTable(Name,PhoneNumber) values ('孙策','1596637558')


----------------------------------
---商品信息数据
select * from GoodsMessage
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI Nova5i','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI P40 ','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI P40 Pro ','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI Mate 30 Pro ','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI Mate 30','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI Mate 30 RS','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( '华为畅享20 Plus ','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( '华为畅享20','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI nova 7','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( '华为畅享20 Pro','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( 'HUAWEI nova 7 Pro ','华为旗舰店','2025-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( '松子','三只松鼠','2021-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( '核桃','三只松鼠','2021-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( '夏威夷果','三只松鼠','2021-10-1')
insert into GoodsMessage(GoodsName , GoodsVende , GoodsExpirationDate) values ( '碧根果','三只松鼠','2021-10-1')

-------------------------

					--申请人ID
					--货品ID
					-- 货品价格
					-- 货品数量
					--货品备注
					--进货时间
--采购申请表数据
select * from PurRqs
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(1,1,3000,200,'手机',getdate())
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(3,1,3000,300,'手机',getdate())
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(5,2,1999,50,'手机',getdate())
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(3,6,1599,100,'手机',getdate())
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(3,5,5999,20,'手机',getdate())
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(6,4,3999,300,'手机',getdate())
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(7,6,699,600,'手机',getdate())
insert into PurRqs(PurRqsNameID , GoodsID ,GoodsMoney , GoodsNumber , GoodsRemark ,PurRqsTime)
	values(8,14,29.9,5000,'零食',getdate())

-------------------------------------------

--订货人ID
--货品ID
--货品价格
--货品数量
--订货时间
select * from OrderGoods
--订货表
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(1,1,3000,500,getdate())
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(3,1,3000,500,getdate())
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(5,2,1999,500,getdate())
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(3,6,1599,500,getdate())
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(3,5,5999,500,getdate())
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(6,4,3999,500,getdate())
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(7,6,699,500,getdate())
insert into OrderGoods(OrderGoodsNameID , GoodsID ,GoodsMoney , GoodsNumder ,OrderGoodsTime)
	values(8,14,29.9,500,getdate())



------------------------------------------------------------------------------------
--库存信息表数据
select * from Repertory
 --库存货品ID
 --库存数量
 --入库时间
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(3,500,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(1,63,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(2,100,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(4,133,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(5,153,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(6,352,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(7,452,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(8,360,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(9,252,getdate())
insert into Repertory(RepertoryNameID , RepertoryNumder , RepertoryInTime)	values(10,101,getdate())


----------------------------------------------------
/*入库货品ID
数量
库存入时间*/

---入库数据
select * from RepertoryInDeal

insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (1,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (2,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (3,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (4,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (5,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (6,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (7,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (8,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (9,500,getdate())
insert into RepertoryInDeal(InRepertoryGoodsID,Numder,RepertoryDealTime) values (10,500,getdate())

----------------------------------------------------
/*出库货品ID
数量
库存出时间*/
select * from RepertoryOutDeal
---出库数据
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (1,437,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (2,400,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (3,500,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (4,367,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (5,347,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (6,148,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (7,48,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (8,140,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (9,248,getdate())
insert into RepertoryOutDeal(OutRepertoryGoodsID,Numder,RepertoryDealTime) values (10,399,getdate())









select * from GoodsMessage
select * from NameTable
select * from OrderGoods
select * from PurRqs
select * from Repertory
select * from RepertoryInDeal
select * from RepertoryOutDeal