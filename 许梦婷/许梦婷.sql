create database purchaseSellStock;
go

use purchaseSellStock;
go

create table StaffList--工作人员表               
(
StaffId int primary key identity(1, 1),
StaffName varchar(10) not null,--工作人员姓名
StaffPhon varchar(30) not null,--工作人员联系电话
);
create table Stock--进货表
 (
	StockId int primary key identity(1, 1),
    ProductName varchar(20) not null,--商品名称
	PrimeCost   money not null,--产品价格
	StockTime smalldatetime not null,--进货时间
	Quantity int not null,--进货数量
	Acceptor varchar(20) not null,--验收人员

);
create table Inventory --入库表
 (
	InventoryId int primary key identity(1, 1),
   InventoryName varchar(20) not null,--入库商品名称
    InventoryTime smalldatetime not null,--入库时间
	InventoryQuantity int not null,--入库数量
	InventoryAdministrator varchar(20) not null,--管理人名称
);
create table Removal --出库表
 (
	RemovalId int primary key identity(1, 1),
    RemovalName varchar(20) not null,--出库商品名称
    RemovalTime smalldatetime not null,--出库时间
	RemovalQuantity int not null,--出库数量
	Administrator varchar(20) not null,--管理人名称
);
create table SalesTable --销量表
 (
	SaleslId int primary key identity(1, 1),
    SalesName varchar(20) not null,--商品名称
  RemovalTime smalldatetime not null,--交易时间
    SalesCost   money not null,--销售价格
	Amount int not null,--数量
	AmountinTotal money not null, --合计价格
	principal  varchar(20) not null,--负责人名称
);

create table ResidueTable--库存表
(
ResidueId int primary key identity(1, 1),
 ResidueName varchar(20) not null,--物品名称
 ResidueAmount int not null,--余量
 WarehouseEntryTime smalldatetime not null, --入库时间
 CountingPersonnel varchar(20) not null,--清点人员
);



--工作人员
select*from StaffList
insert into StaffList (StaffName,StaffPhon )
values ('小红','13554698965')
delete from StaffList where StaffId=2
insert into StaffList (StaffName,StaffPhon )
values ('小刘','13554698649')
insert into StaffList (StaffName,StaffPhon )
values ('小许','13687423564')
insert into StaffList (StaffName,StaffPhon )
values ('小王','15236498546')
--进货表
select*from Stock
insert into Stock (ProductName,PrimeCost,StockTime,Quantity ,Acceptor)
values ('足球','20',GETDATE(),'500','小刘')
insert into Stock (ProductName,PrimeCost,StockTime,Quantity ,Acceptor)
values ('篮球','20',GETDATE(),'600','小刘')
insert into Stock (ProductName,PrimeCost,StockTime,Quantity ,Acceptor)
values ('乒乓球','0.5',GETDATE(),'800','小许')
insert into Stock (ProductName,PrimeCost,StockTime,Quantity ,Acceptor)
values ('乒乓球拍','10',GETDATE(),'400','小许')
insert into Stock (ProductName,PrimeCost,StockTime,Quantity ,Acceptor)
values ('跳绳','8',GETDATE(),'300','小王')
insert into Stock (ProductName,PrimeCost,StockTime,Quantity ,Acceptor)
values ('排球','15',GETDATE(),'500','小红')

--入库表
select*from  Inventory
insert into Inventory (InventoryName,InventoryTime,InventoryQuantity,InventoryAdministrator)
values ('足球',GETDATE(),'500','小刘')
insert into Inventory (InventoryName,InventoryTime,InventoryQuantity,InventoryAdministrator)
values ('篮球',GETDATE(),'600','小刘')
insert into Inventory(InventoryName,InventoryTime,InventoryQuantity,InventoryAdministrator)
values ('乒乓球',GETDATE(),'800','小许')
insert into Inventory (InventoryName,InventoryTime,InventoryQuantity,InventoryAdministrator)
values ('乒乓球拍',GETDATE(),'400','小许')
insert into Inventory (InventoryName,InventoryTime,InventoryQuantity,InventoryAdministrator)
values ('跳绳',GETDATE(),'300','小王')
insert into Inventory(InventoryName,InventoryTime,InventoryQuantity,InventoryAdministrator)
values ('排球',GETDATE(),'500','小红')



--出库表
select*from  Removal
insert into Removal(RemovalName, RemovalTime,RemovalQuantity,Administrator)
values ('足球',GETDATE(),'50','小许')
insert into Removal(RemovalName, RemovalTime,RemovalQuantity,Administrator)
values ('篮球',GETDATE(),'50','小许')
insert into Removal(RemovalName, RemovalTime,RemovalQuantity,Administrator)
values ('乒乓球',GETDATE(),'50','小许')
insert into Removal(RemovalName, RemovalTime,RemovalQuantity,Administrator)
values ('乒乓球拍',GETDATE(),'50','小许')
insert into Removal(RemovalName, RemovalTime,RemovalQuantity,Administrator)
values ('跳绳',GETDATE(),'50','小许')
insert into Removal(RemovalName, RemovalTime,RemovalQuantity,Administrator)
values ('排球',GETDATE(),'50','小许')



--销量表
select*from  SalesTable
insert into SalesTable(SalesName,RemovalTime,SalesCost,Amount,AmountinTotal,principal)
values ('足球',GETDATE(),60,'50',3000,'小许')
insert into SalesTable(SalesName,RemovalTime,SalesCost,Amount,AmountinTotal,principal)
values ('篮球',GETDATE(),60,'50',3000,'小许')
insert into SalesTable(SalesName,RemovalTime,SalesCost,Amount,AmountinTotal,principal)
values ('乒乓球',GETDATE(),1,'50',50,'小许')
insert into SalesTable(SalesName,RemovalTime,SalesCost,Amount,AmountinTotal,principal)
values ('乒乓球拍',GETDATE(),20,'50',100,'小许')
insert into SalesTable(SalesName,RemovalTime,SalesCost,Amount,AmountinTotal,principal)
values ('跳绳',GETDATE(),16,'50',800,'小许')
insert into SalesTable(SalesName,RemovalTime,SalesCost,Amount,AmountinTotal,principal)
values ('排球',GETDATE(),30,'50',1500,'小许')


--库存表
select*from ResidueTable
insert into ResidueTable( ResidueName,ResidueAmount, WarehouseEntryTime, CountingPersonnel)
values ('足球',450,GETDATE(),'小王')
insert into ResidueTable( ResidueName,ResidueAmount, WarehouseEntryTime, CountingPersonnel)
values ('篮球',550,GETDATE(),'小红')
insert into ResidueTable( ResidueName,ResidueAmount, WarehouseEntryTime, CountingPersonnel)
values ('乒乓球',750,GETDATE(),'小许')
insert into ResidueTable( ResidueName,ResidueAmount, WarehouseEntryTime, CountingPersonnel)
values ('乒乓球拍',350,GETDATE(),'小刘')
insert into ResidueTable( ResidueName,ResidueAmount, WarehouseEntryTime, CountingPersonnel)
values ('跳绳',250,GETDATE(),'小许')
insert into ResidueTable( ResidueName,ResidueAmount, WarehouseEntryTime, CountingPersonnel)
values ('排球',450,GETDATE(),'小刘')
