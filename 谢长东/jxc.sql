use master
go
create database InvoicingManagementSystem
go
use InvoicingManagementSystem
go
--------------
--产商信息表
--------------
			
create table ManufacturerInfo
(
ManufacturerId int primary key identity(1,1),--产商id
ManufacturerName nvarchar(50) not null,--产商名称
ManufacturerAddres nvarchar(50) not null,--产商地址
ManufacturerPhone varchar(20)	not null--产商联系电话
)

-----------------
--产商商品信息表
-----------------
		
create table CommodityInfo
(
CommodityId int primary key identity(1,1),--商品id
ManufacturerId int not null,--产商id
CommodityName varchar(30) not null,--商品名称
CommodityPrice money not null--商品价格

)

----------------------
--岗位类型表
---------------------
	
create table JobsType
(
JobsId tinyint primary key identity(1,1),--岗位id
JobsName nvarchar(20) not null--岗位名称（普通员工、销售经理）
)

--------------------
--员工信息表
--------------------
						
create table EmployeesInfo
(
EmployeesId int primary key identity(1,1),--员工id
JobsId tinyint not null,--岗位id
EmployeesName nvarchar(10) not null,--姓名
sex	char(2) not null,--性别
EmployeesAddres nvarchar(50) not null,--住址
EmployeesPhone varchar(20) not null,--联系电话
WageCardNO varchar(30) not null--工资卡号
)

-------------
--进货订单表
-------------
		 	  	 				
create table PurchaseOrder
(
OrderId int primary key identity(1,1),--订单id
EmployeesId int not null,--员工id
CommodityId int not null,--商品id
Quantity int not null,--数量
TotalMoney money not null,--总金额
ShippingAddress nvarchar(50) not null,--收货地址
OrderTime smalldatetime default getdate()--下单时间
)


-----------------
--商品库存信息表
-----------------
							
create table InventoryInfo
(
CommodityId int primary key identity(1,1),--商品id
CommodityName varchar(30) not null,--商品名称
CostPrice money not null,--成本价格
Inventory int not null,--库存
InventoryMoney money not null,--库存金额
InventoryMin int not null,--库存下限
InventoryMax int not null,--库存上限
StorageTime smalldatetime default getdate()--入库时间
)
----------------
--商品销售订单记录表
---------------
							  		
create table SalesOrder
(
OrderId int primary key identity(1,1),--订单id
CommodityId int not null,--商品id
SalesPrice money not null,--销售价格
Quantity int not null,--数量
TotalMoney money not null,--总金额
EmployeesId int not null,--销售员工id
ClientId int not null,--客户id
ShippingAddress nvarchar(50) not null,--收货地址
OrderTime smalldatetime default getdate()--下单时间
)

-----------
--销售订单状态记录表
----------		
create table SalesOrderState
(
StateId int primary key identity(1,1),--状态id
OrderId int not null,--订单id
StateType varchar(50) not null,--状态类型（已支付、待支付、运输中、交易完成、退换货）
ChangeTime smalldatetime default getdate()--变动时间
)

----------------
--退货记录表
---------------
-- 订单id	退货理由	时间
create table SalesReturnRecord
(
OrderId int not null,
SalesReturnReason nvarchar(100) not null,
SalesReturnTime smalldatetime default getdate()
)


-----------------
--客户信息表
----------------
--客户id	客户姓名	身份证号	手机号码
create table ClientInfo
(
ClientId int primary key identity(1,1),
ClietName varchar(10) not null,
IDNumber varchar(20) not null,
PhoneNumber varchar(11) not null

)

-----------------
--客户账号信息表
------------------
--账号id	客户id	账号	密码
create table ClientIDInfo
(
AccountId int primary key identity(1,1),
ClientId int not null,
Account varchar(20) not null,
AccountPw varchar(20) not null

)











alter table CommodityInfo add constraint FK_CommodityInfo_ManufacturerId foreign key(ManufacturerId) references ManufacturerInfo(ManufacturerId)


alter table EmployeesInfo add constraint FK_EmployeesInfo_JobsId foreign key(JobsId) references JobsType(JobsId)


alter table PurchaseOrder add 
constraint FK_PurchaseOrder_EmployeesId foreign key(EmployeesId) references EmployeesInfo(EmployeesId),
constraint FK_PurchaseOrder_CommodityId foreign key(CommodityId) references CommodityInfo(CommodityId)

alter table SalesOrder add 
constraint FK_SalesOrder_EmployeesId foreign key(EmployeesId) references EmployeesInfo(EmployeesId),
constraint FK_SalesOrder_CommodityId foreign key(CommodityId) references InventoryInfo(CommodityId),
constraint FK_SalesOrder_ClientId foreign key(ClientId) references ClientInfo(ClientId)

alter table SalesOrderState add 
constraint FK_SalesOrderState_OrderId foreign key(OrderId) references SalesOrder(OrderId)

alter table ClientIDInfo add constraint FK_ClientIDInfo_ClientId foreign key(ClientId) references ClientInfo(ClientId)


alter table SalesReturnRecord add constraint FK_SalesReturnRecord_OrderId foreign key(OrderId) references SalesOrder(OrderId)