 create database InvoicingSystem

 go

 use InvoicingSystem 

 go


 create table Goods (GoodsId int primary key identity (1,1),--商品表
                     GoodsName varchar(30),
					 GoodsNum int not null ,
					 GoodsPrice int not null
					 )




create table Category (CategoryId int primary key identity (1,1),--商品分类表
                       GoodsId int,
					   Sort varchar(20) not null )



create table InWarehouse (InWarehouseId int primary key identity (1,1),--入库表
                          GoodsId int,
						  UsersId int ,
						  InWarehouseTime smalldatetime,
						  GoodsNum int not null )

create table OutWarehouse (OutWarehouseId int primary key identity (1,1),--出库表
                          GoodsId int,
						  UsersId int,
						  OutWarehouseTime smalldatetime,
						  GoodsNum int not null )

create table Orders (OrdersId int primary key identity (1,1),--订单表
                     GoodsId int,
					 UserId int,
					 HarvestAddress varchar(30) not null,
					 TransactionTime smalldatetime)


create table Users(UsersId int  primary key identity (1,1),--人员表
                   UsersName varchar(30))



insert into Goods (GoodsName,GoodsNum ,GoodsPrice ) values ('篮球',101,95),('排球',150,65),('乒乓球',200,5)
insert into Users (UsersName ) values ('胡小胖'),('刘华森'),('叶长春')
insert into Category(GoodsId ,Sort  ) values (1,'体育')
insert into InWarehouse (GoodsId,UsersId ,InWarehouseTime ,GoodsNum ) values (1,1,GETDATE (),66),(2,3,GETDATE (),30)
insert into OutWarehouse (GoodsId ,UsersId ,OutWarehouseTime ,GoodsNum ) values (1,2,GETDATE (),5),(2,2,GETDATE (),15)
insert into  Orders (GoodsId ,UsersId ,HarvestAddress ,TransactionTime ) values (3,3,'福建林厝村有限公司',GETDATE ())


alter table Inwarehouse add constraint PK_Inwarehouse_PK1 foreign key (GoodsId) references Goods(GoodsId)
alter table Category add constraint PK_Inwarehouse_PK2 foreign key (GoodsId) references Goods(GoodsId)
alter table Inwarehouse add constraint PK_Inwarehouse_PK3 foreign key (UsersId) references Users(UsersId)
alter table OutWarehouse add constraint PK_Inwarehouse_PK4 foreign key (UsersId) references Users(UsersId)
alter table OutWarehouse add constraint PK_Inwarehouse_PK5 foreign key (GoodsId) references Goods(GoodsId)
alter table Orders add constraint PK_Inwarehouse_PK6 foreign key (GoodsId) references Goods(GoodsId)
alter table Orders add constraint PK_Inwarehouse_PK7 foreign key (UsersId) references Users(UsersId)


select * from Goods 
select * from Category 
select * from InWarehouse 
select * from Orders 
select * from OutWarehouse 
select * from Users 