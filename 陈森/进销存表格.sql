create database XiaoBaiSystem
use XiaoBaiSystem


 



 --采购：供应商 订单 采购单 付款单 退货单 费用支出单。。。
 --销售：客户 订单 销售单 收款单 退货单 。。。
 --仓存：存货 采购单 销售单 采购退货 销售退货。。。 


create table Supplier/*供应商表*/
(SupplierId int primary key identity (1,1),--供应商编号
SupplierName Nvarchar(100)not null,  --供应商名字  
SupplierPhome Varchar(20)not null,--供应商电话号码
SupplierAddress Nvarchar(100)not null,)--供应商地址 


create table Users /*用户表/客户表*/
(UserId int primary key identity (1,1),--用户ID
UserPhome Varchar(20) not null,--用户手机号码
UserName Nvarchar(50)not null,--用户名字
UserSex tinyint , --用户性别
UserAddress  nvarchar(100)not null,--用户地址 
)
create table Employee  /* 员工表 */  
 (   
   EmployeeId     int identity(1,1)   NOT NULL,  --员工编号   
   EmployeeName             varchar(30)       NOT NULL,  -- 姓名   
   EmployeeDuty             varchar(20)       NOT NULL,  -- 职务   
   Sex                      tinyint   ,                    -- 性别  
   EmployeeBirthDate        datetime          NOT NULL,  -- 出生日期   
   EmployeeHireDate         datetime          NULL,      -- 合同签订 日期   
   EmployeeMatureDate       datetime          NULL,     --  合同到期日  
   EmployeeIdentityCard     varchar(20)       NULL,     --  身份证号  
   EmployeeAddress          varchar(250)      NULL,     --  住址
   EmployeePhone            varchar(25)       NULL, --      电话   

)  


create table  Department/*部门表*/
(
DepartmentId int identity (1,1),--部门编号Id
DepartmentName nvarchar(50),--部门名字
DepartmentDuty nvarchar (100),--部门职务，
EmployeeId int not null ,--员工所属部门
 )

create table WareHouse/*仓库表*/
(
WareHouseId int identity(1,1),--仓库编号
WareHouseTime datetime ,--仓库成立时间
WareHousePhome varchar(50) not null,--仓库联系人
WareHouseStockId int not null,--进货仓库编号
WareHouseRetreatId int not null,--退货仓库

)

create table Purchase/*采购表入库*/
(PurchaseId int identity (1,1),--采购Id
PurchaseName Nvarchar(1000),--采购商品名字
PurchasePhome varchar(20),--采购联系人
PurchaseDAte datetime ,--采购日期
WareHouseId int not null,--进库编号
)

create table LeaveStock /*出库表*/
(LeaveStockId int identity (1,1)not null,--出库编号
 leaveStockTime datetime ,--出库时间
 leaveStockPhome varchar(20),--出库负责人
 WareHouseId  int not null,--出库所在仓库
 DepartmentId int not null ,--出库负责部门
 ) 
 
create table Inventory /*库存表*/
(InventoryId int identity (1,1),--ID
WareHouseId int not null ,--所在仓库
CommodityId int not null,--商品编号
)

 
create table Dingdan/*订单表*/
(DingdanId int identity (1,1),--订单ID
Dingtime smalldatetime,--下单时间
DingAffirm varchar(1000),--确认订单
DingCancel varchar(1000),--取消订单 
)


create table Payment /*付款表*/
(
PaymentId int Identity (1,1) not null,--Id
PaymentTime datetime,--付款时间
PaymentUser varchar(100),--用户付款
PaymentPurchase varchar(100),--采购单付款
PaymentSupplier Varchar(100),--供应商付款


 )
 create table Refund /*退款表*/
(
RefundId int Identity (1,1) not null,--Id
RefundUser  VArchar(100),--用户退款
RefundPurchase varchar(100),--采购单退款
RefundSupplier varchar(100),--供应商退款

 )
create table PaymentDetails/*付款明细表*/
(PaymentDetailsId int identity (1,1),--Id自增
PaymentDetailsUser money not null,--用户
PaymentDetailsSupplier money ,--供应商
PaymentDetailsPurchase money --采购单
)
create table Market/*销售表*/
(
MarketId int identity (1,1),--Id
MarketPeriod smalldatetime ,--周期销售额
MarketMonth smalldatetime,--月季度
 )


create  table Commodity/*商品表*/
(
CommodityId int identity (1,1),--商品ID
CommodityClassify int not null,--商品分类Id
CommodityName nvarchar (1000),--商品名字
CommodityPrice money ,--商品价格
EmployeeId int not null,--操作人员
WareHouseId int not null,--商品所属仓库
)
create table CommodityClassify /*商品总分类表*/
(
CommodityClassifyId int primary key  identity (1,1),--id
CommodityClassifyName nvarchar(1000),--商品分类名字/品牌
CommodityClassifyTmie dateTime ,--分类时间
CommodityClassifyRemark Varchar (100),--备注，描述 
CommodityPhome char(20) not null,--分类负责人
)
 
 
 
