--进销存库
Create database Warehouse
go
use Warehouse
go

--部门表(部门表Id,部门名字)
Create table Department
(
	DepartmentId int identity(1,1) primary key not null,
	DepartmentName nvarchar(20) not null
)
go
--员工表（员工Id,员工姓名,部门Id,员工职务,员工性别,员工身份证,员工出生日期,员工住址,员工电话，员工邮箱）
Create table Staff
(
	StaffId int identity(1,1) primary key not null,
	StaffName nvarchar(20) not null,
	DepartmentId int not null,   --部门表(1)
	Duty nvarchar(20) not null,
	StaffSex varchar(2) not null,
	StaffCode varchar(30) not null,
	BirthDate smalldatetime not null,
	StaffAddress nvarchar(50) not null,
	StaffPhone varchar(20) not null,
	E_Mail nvarchar(20) not null
)
go
--用户表（用户Id,用户姓名,身份证号,性别,用户电话,地址）
Create table Users
(
	UserId int identity(1,1) primary key not null,
	UserName nvarchar(20) not null,
	UserCode varchar(20) not null,
	UserSex varchar(2) not null,
	UserPhone varchar(30) not null,
	UserAddress nvarchar(200) not null
)
go
--商品供应商表
Create table Commodity_Supplier
(
	Commodity_SupplierId int identity(1,1) primary key not null,
	CommodityId int not null, --商品表
	SupplierId int not null  --供应商表
)
go
--商品类型表
Create table Commodity_Type
(
	Commodity_TypeId int identity(1,1) primary key not null,
	TypeName nvarchar(20) not null
)
go
--状态表
Create table Commodity_State
(
	Commodity_StateId int identity(1,1) primary key not null,
	StateName nvarchar(20) not null
)
--原因表
Create table Cause
(
	CauseId int identity(1,1) primary key not null,
	Describe nvarchar(50) not null
)

--商品表(商品Id,商品名称,商品价格,商品描述,商品生产日期)
Create table Commodity
(
	CommodityId int identity(1,1) primary key not null,
	CommodityName nvarchar(30) not null,
	CommodityPrice money  not null,
	Describe nvarchar(50) not null,
	ProductionDate smalldatetime not null
)
go
--供应商表(供应商Id,供应商名称,供应商联系方式,供应商地址,供应商联系人)
Create table Supplier
(
	SupplierId int identity(1,1) primary key not null,
	SupplierName nvarchar(30) not null,
	SupplierPhone varchar(30) not null,
	SupplierAddress nvarchar(50) not null,
	LiaisonMan nvarchar(30) not null
)
go
--进货记录表（进货Id,商品Id,供应商Id,进货数量,状态,商品进货时间）
Create table PurchaseRecord
(
	PurchaseRecordId int identity(1,1) primary key not null,
	CommodityId int not null,  --商品表
	Number int not null,
	SupplierId int not null,  --供应商表
	Commodity_TypeId int not null, --类型表
	PurchaseDate smalldatetime not null
)
go
--商品出售记录表(商品出售记录Id,商品Id,用户Id,商品数量,类型Id,商品销售时间)
Create table SellRecords
(	
	SellRecordsId int identity(1,1) primary key not null,
	CommodityId int not null,  --商品表
	UserId int not null,  --用户表
	Number int not null,
	Commodity_TypeId Int not null, --类型表
	SellDate smalldatetime not null
)
go
--商品库存表（商品库存Id,商品Id,商品数量,状态Id）
Create table Stock
(
	StockId int identity(1,1) primary key not null,
	CommodityId int not null,  --商品表
	StockNumber int not null,
	Commodity_StateId int not null  --状态表
)
go
--出库记录表(出库Id,商品Id,商品数量,类型Id,原因,出库时间)
Create table OutWarehouse
(
	OutWarehouseId int identity(1,1) primary key not null,
	CommodityId int not null,  --商品表
	Number int not null,
	Commodity_TypeId int not null,  --类型表
	CauseId int not null, --原因表
	OutWarehouseTime smalldatetime not null
)
go
--入库记录表(入库Id,商品Id,商品数量,类型Id,原因,入库时间)
Create table InWarehouse
(
	InWarehouseId int identity(1,1) primary key not null,
	CommodityId int not null,  --商品表
	Number int not null,
	Commodity_TypeId int not null,  --类型表
	CauseId int not null,	--原因表
	InWarehouseTime smalldatetime not null
)
go





--出库表的 原因Id
alter table OutWarehouse add constraint FK_OutWarehouse_CauseId foreign key(CauseId)  references Cause(CauseId)
--入库表的 原因Id
alter table InWarehouse add constraint FK_InWarehouse_CauseId foreign key(CauseId)  references Cause(CauseId)
--员工表的 部门Id
alter table Staff add constraint FK_Staff_DepartmentId foreign key(DepartmentId)  references Department(DepartmentId)
--商品供应商表的 商品Id
alter table Commodity_Supplier add constraint FK_Commodity_Supplier_CommodityId foreign key(CommodityId)  references Commodity(CommodityId)
--商品供应商表的 供应商Id
alter table Commodity_Supplier add constraint FK_Commodity_Supplier_SupplierId foreign key(SupplierId)  references Supplier(SupplierId)
--进货表的 商品Id
alter table PurchaseRecord add constraint FK_PurchaseRecord_CommodityId foreign key(CommodityId)  references Commodity(CommodityId)
--进货表的 供应商Id
alter table PurchaseRecord add constraint FK_PurchaseRecord_SupplierId foreign key(SupplierId)  references Supplier(SupplierId)
--进货表的 类型Id
alter table SellRecords add constraint FK_PurchaseRecord_Commodity_TypeId foreign key(Commodity_TypeId)  references Commodity_Type(Commodity_TypeId)
--出售表的 商品Id
alter table SellRecords add constraint FK_SellRecords_CommodityId foreign key(CommodityId)  references Commodity(CommodityId)
--出售表的 用户Id
alter table SellRecords add constraint FK_SellRecords_UserId foreign key(UserId)  references Users(UserId)
--库存表的 商品Id
alter table Stock add constraint FK_Stock_CommodityId foreign key(CommodityId)  references Commodity(CommodityId)
--出售表的 类型Id
alter table SellRecords add constraint FK_SellRecords_Commodity_TypeId foreign key(Commodity_TypeId)  references Commodity_Type(Commodity_TypeId)
--库存表的 状态Id
alter table Stock add constraint FK_Stock_Commodity_StateId foreign key(Commodity_StateId)  references Commodity_State(Commodity_StateId)
--出库表的 商品Id
alter table OutWarehouse add constraint FK_OutWarehouse_CommodityId foreign key(CommodityId) references Commodity(CommodityId)
--出库表的 类型Id
alter table OutWarehouse add constraint FK_OutWarehouse_Commodity_TypeId foreign key(Commodity_TypeId) references Commodity_Type(Commodity_TypeId)
--入库表的 商品Id
alter table InWarehouse add constraint FK_InWarehouse_CommodityId foreign key(CommodityId)  references Commodity(CommodityId)
--入库表的 类型Id
alter table InWarehouse add constraint FK_InWarehouse_Commodity_TypeId foreign key(Commodity_TypeId)  references Commodity_Type(Commodity_TypeId)
